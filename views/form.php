<div class="editor">
<?= CHtml::beginForm($_SERVER['REQUEST_URI'], 'post') ?>
	<table cellpadding="0" cellspacing="0" class="form_table">
		<?
		/**
		 * @var AbstractInput $element
		 */
		foreach($formElements as $element){ ?>
		<tr>
			<td class="title"><?= EditorHelper::activeLabel($this->model, $element->getModelFieldName(), array(), $element->getMultilingual()) ?>:</td>
			<td class="is_null">
				<?= CHtml::checkBox('editor[is_null][' . $element->getModelFieldName() . ']', $element->isNull(),
					array(
						'disabled' => $element->getAllowNull()===true ? '' : 'disabled',
						'class' => 'is_null_input'
					)
				) ?>
			</td>
			<td class="element <?= $element->isNull()===true ? 'latent' : '' ?>"><?= $element->input() ?></td>
		</tr>
		<? } ?>
	</table>
	<?= CHtml::submitButton(Yii::t('editor', 'submit')) ?>
<?= CHtml::endForm() ?>
</div>