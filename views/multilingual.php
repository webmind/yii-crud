<?
// Получаем массив языков сайта
$languages = explode(',', Yii::app()->params['languages']);
// Генерируем кнопки языков
foreach($languages as $key => $languageName) {
	echo CHtml::tag('div',
		array(
			// Выделям кнопку первого языка
			'class' => $key!=0 ? 'flag16 flag16' . $languageName : 'flag16 flag16' . $languageName . ' ' . 'flag_selected',
		),
		$languageName
	);
}
?>
<div style="clear: both;margin: 0 0 2px 0;"></div>
<?
// Для каждлго языка генерируем элемент
foreach($languages AS $key => $languageName) {
	echo CHtml::tag('div',
		array(
			// Делаем видимым только первый элемент
			'style' => $key!=0 ? 'display:none;' : '',
			'class' => 'input input_' . $languageName
		),
		// Вызываем метод котоый возвращает HTML элемента
		$this->createInput($languageName)
	);
}
?>