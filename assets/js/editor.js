$(document).ready(function(){
	$('.editor .isNull input').click(function() {
		if($(this).attr('checked')){
			$(this).parent().parent().find('.editArea').find('option').removeAttr("selected");
		}
	});

	$('.editArea select').click(function (){
		$(this).parent().parent().find('.isNull input').attr('checked',false);
	});

	/**
	 * Клик на одном из флагов над полем
	 */
	$('.flag16').live('click', function (){
		/**
		 * Класы которые привязаны к флагу
		 * @type {*|number|jQuery}
		 */
		var classes = $(this).attr('class').split(' ');
		/**
		 * Все инпути делаем невидимыми
		 */
		$(this).parent().find('.input').each(function() {
			$(this).css('display', 'none');
		});

		/**
		 * Делаем видимым тот инпут который соотвествует кнопке
		 */
		$(this).parent().find('.input_' + classes[1].substr(-2)).css('display', '');

		/**
		 * Все флаги делаем невыделенными
		 */
		$(this).parent().find('.flag16').each(function() {
			$(this).removeClass('flag_selected');
		});

		/**
		 * Делаем выделенным тот флаг по которому кликнули
		 */
		$(this).addClass('flag_selected');
	});

	/**
	 * Открытие закрывтия списка
	 */
	$('.editor .openHideEditArea').click(function() {
		if ($(this).parent().find('.editArea').css('display') == 'none') {
			$(this).parent().find('.editArea').css('display', 'block');
		} else {
			$(this).parent().find('.editArea').css('display', 'none');
		}
	}).button();

	/**
	 * Клик на флаг is_null
	 */
	$('.editor input.is_null_input').click(function () {
		if ($(this).attr('checked') == 'checked') {
			$(this).parent().parent().find(".element").addClass("latent");
		} else {
			$(this).parent().parent().find(".element").removeClass("latent");
		}
	});

    $('.generate_seo').click(function () {
        var val = $('input:regex(id, editor_name*)').val();
        $(this).parent().find('input').val(val);
    });

    $('.generate_sef').click(function () {
        var val = $('input:regex(id, editor_name*)').val();
        $(this).parent().find('input').val(val.translit().toLowerCase());
    });
});

String.prototype.translit = (function(){
    var L = {
            'А':'A','а':'a','Б':'B','б':'b','В':'V','в':'v','Г':'G','г':'g',
            'Д':'D','д':'d','Е':'E','е':'e','Ё':'Yo','ё':'yo','Ж':'Zh','ж':'zh',
            'З':'Z','з':'z','И':'I','и':'i','Й':'Y','й':'y','К':'K','к':'k',
            'Л':'L','л':'l','М':'M','м':'m','Н':'N','н':'n','О':'O','о':'o',
            'П':'P','п':'p','Р':'R','р':'r','С':'S','с':'s','Т':'T','т':'t',
            'У':'U','у':'u','Ф':'F','ф':'f','Х':'Kh','х':'kh','Ц':'Ts','ц':'ts',
            'Ч':'Ch','ч':'ch','Ш':'Sh','ш':'sh','Щ':'Sch','щ':'sch','Ъ':'"','ъ':'"',
            'Ы':'Y','ы':'y','Ь':"'",'ь':"'",'Э':'E','э':'e','Ю':'Yu','ю':'yu',
            'Я':'Ya','я':'ya',' ':'-'
        },
        r = '',
        k;
    for (k in L) r += k;
    r = new RegExp('[' + r + ']', 'g');
    k = function(a){
        return a in L ? L[a] : '';
    };
    return function(){
        return this.replace(r, k);
    };
})();

jQuery.expr[':'].regex = function(elem, index, match) {
    var matchParams = match[3].split(','),
        validLabels = /^(data|css):/,
        attr = {
            method: matchParams[0].match(validLabels) ?
                matchParams[0].split(':')[0] : 'attr',
            property: matchParams.shift().replace(validLabels,'')
        },
        regexFlags = 'ig',
        regex = new RegExp(matchParams.join('').replace(/^\s+|\s+$/g,''), regexFlags);
    return regex.test(jQuery(elem)[attr.method](attr.property));
}