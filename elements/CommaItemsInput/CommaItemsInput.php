<?php

class CommaItemsInput extends AbstractInput{

	private $relationName;

	private $relationView;

	private $separator = ',';

	public function createInput($language = null, $params=array()) {
		$value = '';
		$isFirst = true;
		foreach($this->getModel()->{$this->relationName} AS $v) {
			if ( ! $isFirst) {
				$value .= $this->separator;
			}
			$value .= $v->{$this->relationView};
			$isFirst = false;
		}
		return CHtml::textArea('editor[' . $this->relationName . ']', $value, $params);
	}

	public function save() {
		return $this->getModel();
	}

	public function afterSave() {
		$relationClass = $this->getModel()->relations()[$this->relationName][1];
		$relationColumn = $this->getModel()->relations()[$this->relationName][2];
		$values = explode($this->separator, $_POST['editor'][$this->relationName]);
		foreach($this->getModel()->{$this->relationName} AS $v) {
			$v->delete();
		}
		foreach ($values AS $v) {
			$item = new $relationClass();
			$item->{$this->relationView} = $v;
			$item->{$relationColumn} = $this->getModel()->id;
			$item->save();
		}
	}

	public function getRelationView() {
		return $this->relationView;
	}

	public function setRelationView($relationView) {
		$this->relationView = $relationView;
	}

	public function getRelationName() {
		return $this->relationName;
	}

	public function setRelationName($relationName) {
		$this->relationName = $relationName;
	}

	public function getSeparator() {
		return $this->separator;
	}

	public function setSeparator($separator) {
		$this->separator = $separator;
	}
}
