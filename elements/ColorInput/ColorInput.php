<?php
/**
 * Created: 01.02.13 15:34
 * 
 * @author tcore
 */

class ColorInput extends AbstractInput{

	/**
	 * @var string имя столбца в БД
	 */
	private $columnName;

	public function createInput($language = null, $params=array()) {
		$this->registerAssets();
		$params = array_merge(array(
			'name' => 'editor[' . $this->getColumnName() . ']',
			'id' => 'ID_' . $this->getColumnName(),
			'value' => '#' . $this->getModel()->{$this->getColumnName()}
		), $params);
		return $this->render('color', array(
			'columnName' => $this->getColumnName(),
			'params' => $params
		), true);
	}

	/**
	 * @return string
	 */
	public function getColumnName() {
		return $this->columnName;
	}

	/**
	 * @param string $columnName
	 */
	public function setColumnName($columnName) {
		$this->columnName = $columnName;
	}

	public function registerAssets() {
		$assets = Yii::app()->assetManager->publish(
			Yii::getPathOfAlias('yii-crud.elements.ColorInput.assets.*'), false, -1, YII_DEBUG
		);
		Yii::app()->getClientScript()->registerCssFile($assets . '/jquery.minicolors.css');
		Yii::app()->getClientScript()->registerScriptFile($assets . '/jquery.minicolors.js');
	}

	public function save() {
		if (isset($_POST['editor'][$this->getColumnName()]) &&
			substr($_POST['editor'][$this->getColumnName()], 0, 1) == '#') {
			$_POST['editor'][$this->getColumnName()] = substr($_POST['editor'][$this->getColumnName()], 1, strlen($this->getColumnName()) + 1);
			return parent::save();
		} else {
			Yii::app()->MessageUtil->add('Error while saving data for color input', 'error');
			return $this->getModel();
		}
	}
}
