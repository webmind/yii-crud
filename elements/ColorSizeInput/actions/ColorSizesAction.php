<?php

Yii::import('yii-crud.components.IInput');
Yii::import('yii-crud.components.AbstractInput');
Yii::import('yii-crud.elements.ColorSizeInput.ColorSizeInput');

class ColorSizesAction extends CAction {

	public function run() {
		if (isset($_GET['size_group_id'])) {
			if ($_GET['size_group_id'] == -1) {
				$criteria = new CDbCriteria(array(
					'with' => 'sizes',
					'limit' => 1,
				));
			} else  {
				$criteria = new CDbCriteria(array(
					'with' => 'sizes',
					'condition' => 't.id = :sizeGroupId',
					'params' => array(
						':sizeGroupId' => $_GET['size_group_id'],
					),
				));
			}
			$sizeGroup = CatalogSizeGroup::model()->find($criteria);
			if ($sizeGroup !== null) {
				$sizes = $sizeGroup->sizes;
			} else {
				return;
			}
		} else {
			$criteria = new CDbCriteria(array(
				'with' => 'catalogSize',
				'condition' => 't.catalog_item_id = :itemId AND t.catalog_color_id = :colorId',
				'params' => array(
					':itemId' => $_GET['item_id'],
					':colorId' => $_GET['color_id']
				),
			));
			$catalogItemColors = CatalogItemColor::model()->findAll($criteria);
			$sizes = array();
			foreach ($catalogItemColors AS $itemColor) {
				$sizes[] = $itemColor->catalogSize;
			}
		}


		$data = array();
		if (isset($_GET['item_id'])) {
			$colors = CatalogItemColor::model()->findAll(array(
				'condition' => 'catalog_item_id = :catalog_item_id AND catalog_color_id = :catalog_color_id',
				'params' => array(':catalog_item_id' => $_GET['item_id'], ':catalog_color_id' => $_GET['color_id']),
			));
			foreach ($colors AS $color) {
				$data[$color->catalog_size_id] = $color->count;
			}
		}

		echo ColorSizeInput::createEditArea($sizes, $_GET['color_id'], $data);
	}
}