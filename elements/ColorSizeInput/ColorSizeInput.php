<?php

class ColorSizeInput extends AbstractInput{

	/**
	 * Список цветов которые есть в системе
	 * @var array
	 */
	private $_colors = array();

	/**
	 * Список групп размеров которые есть в системе
	 * @var array
	 */
	private $_sizeGroups = array();

	/**
	 * Создает єлемент
	 * @param strung|null $language
	 * @param array $params
	 * @return string
	 */
	public function createInput($language = null, $params = array()) {
		$itemColors = $this->getModel()->catalogItemColors;
		$itemColorsArr = array();
		foreach ($itemColors AS $itemColor) {
			if ( ! isset($itemColorsArr[$itemColor->catalog_color_id])) {
				$itemColorsArr[$itemColor->catalog_color_id] = $itemColor->catalogColor->name_ru;
			}
		}

		$this->render('colorSize', array(
			'sizeGroups' => CHtml::dropDownList('', '', $this->_sizeGroups, array('id' => 'sizeGroups')),
			'color_arr' => $itemColorsArr,
			'colors' => CHtml::dropDownList('', '', $this->_colors, array('id' => 'colors')),
			'item_id' => $_GET['id'],
		));
	}

	public function save() {
		return $this->getModel();
	}

	public function afterSave() {
		if (isset($_POST['_colorSize'])) {

			foreach ($this->getModel()->catalogItemColors AS $color) {
				if ( ! $color->delete()) {
					Yii::app()->messageUtil->add($color->getErrors(), 'error', false);
				}
			}

			foreach ($_POST['_colorSize'] AS $colorId => $sizes) {
				foreach ($sizes AS $size => $count) {
					$item = new CatalogItemColor();
					$item->catalog_item_id = $this->getModel()->id;
					$item->catalog_color_id = $colorId;
					$item->catalog_size_id = (int) $size;
					$item->count = $count;
					if ( ! $item->save() ) {
						Yii::app()->messageUtil->add($item->getErrors(), 'error', false);
					}
				}
			}
		}
	}

	public static function createEditArea($sizes, $color_id, $data = array()) {
		$html = '<input type="hidden" value="' . $color_id . '" class="tab_color_id" /><table>';
		foreach ($sizes AS $size) {
			if ($size != null) {
				$value = isset($data[$size->id]) ? $data[$size->id] : 0;
				$html .= '<tr><td style="width: 1px;">' . $size->name .'</td>';
				$html .= '<td><input type="text" name="_colorSize[' . $color_id . '][' . $size->id . ']" value="' . $value . '" /></td>';
				$html .= '</tr>';
			}
		}
		$html .= '</table>';
		return $html;
	}

	public function __construct() {
		parent::__construct();

		$colors = CatalogColor::model()->findAll();
		foreach ($colors AS $color) {
			$this->_colors[$color->id] = $color->name_ru;
		}

		$sizeGroups = CatalogSizeGroup::model()->findAll();
		foreach ($sizeGroups AS $sizeGroup) {
			$this->_sizeGroups[$sizeGroup->id] = $sizeGroup->name;
		}
	}

	public function getColumnName () {
		return 'colorSize';
	}
}