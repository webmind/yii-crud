<?= $sizeGroups ?> <input type="button" value="Изменить категорию" id="change_group"/>
<input type="hidden" id="hidden_category_id" value="-1" />
<?= $colors ?> <input type="button" value="Добавить цвет" id="add_color" />

<script type="text/javascript">
    $(document).ready(function () {

        var colors_arr = jQuery.parseJSON('<?= json_encode($color_arr) ?>');
        var tabs = $('#tabs').tabs();

        tabs.delegate( "span.ui-icon-close", "click", function() {
            var panelId = $( this ).closest( "li" ).remove().attr( "aria-controls" );
            $( "#" + panelId ).remove();
            tabs.tabs( "refresh" );
        });

        for (var color in colors_arr) {
            addTab(tabs, color, colors_arr[color], false);
        }

        $('#add_color').click(function () {
            addTab(tabs, -1, '');
            var color = $('#colors').val();
            $('.imageInput select').val(color);
        });

        $('#change_group').click(function () {
            if ( ! confirm('Все данные в табах будут перезагружены, продолжить?')) {
                $('#sizeGroups').val($('#hidden_category_id').val());
                return;
            }
            var selectedCategoryId = $('#sizeGroups').find(":selected").val();
            $('#hidden_category_id').val(selectedCategoryId);

            $('#tabs').find('div').each(function () {
                var t = this;
                var color_id = $(this).find('.tab_color_id').val();
                var category_id = $('#hidden_category_id').val();
                var url = '?r=default/editor.ColorSizeInput-ColorSizes&size_group_id=' + category_id + '&color_id=' + color_id + '&item_id=<?= $item_id ?>';
                $.ajax(url).
                    done(function (html) {
                        $(t).html(html);
                    });
            });
        });
    });

    function addTab(tabs, selectedColorId, selectedColorText, senSizeGroupId) {
        if (selectedColorId == -1) {
            selectedColorId = $('#colors').find(":selected").val();
            selectedColorText = $('#colors').find(":selected").text();
        }

        if ($('#tab-color-' + selectedColorId).length > 0) {
            alert(selectedColorText + ' цвет уже добавлен');
            return;
        }
        if (senSizeGroupId == false) {
            var url = '?r=default/editor.ColorSizeInput-ColorSizes&color_id=' + selectedColorId + '&item_id=<?= $item_id ?>';
        } else {
            var url = '?r=default/editor.ColorSizeInput-ColorSizes&size_group_id=' + $('#hidden_category_id').val() + '&color_id=' + selectedColorId + '&item_id=<?= $item_id ?>';
        }
        $.ajax(url).
            done(function (html) {
                tabs.find('ul').append(
                    $('<li />').
                        append($('<a />').
                            attr('href', '#tab-color-' + selectedColorId).
                            text(selectedColorText)).
                        append($('<span />').
                            attr('role', 'presentation').
                            attr('class', 'ui-icon ui-icon-close').
                            text('Remove Tab'))
                );
                tabs.append($('<div />').attr('id', 'tab-color-' + selectedColorId).html(html));
                tabs.tabs('refresh');
            });
    }
</script>

<div id="tabs">
    <ul>
    </ul>
</div>