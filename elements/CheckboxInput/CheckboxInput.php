<?php
/**
 * Created: 05.02.13 22:29
 * 
 * @author tcore
 */
 
class CheckboxInput extends AbstractInput{

	/**
	 * @var string имя столбца в БД
	 */
	private $columnName;

	public function createInput($language = null, $params = array()) {
		$params = array_merge(array('name' => 'editor[' . $this->getColumnName() . ']'));
		return CHtml::activeCheckBox($this->getModel(), $this->getColumnName(), $params);
	}

	public function save() {
		$columnName = $this->getColumnName();
		$value = 0;
		if (isset($_POST['editor'][$columnName]) && $_POST['editor'][$columnName] == 1) {
			$value = 1;
		}
		$this->getModel()->$columnName = $value;
		return $this->getModel();
	}

	/**
	 * @return string
	 */
	public function getColumnName() {
		return $this->columnName;
	}

	/**
	 * @param string $columnName
	 */
	public function setColumnName($columnName)
	{
		$this->columnName = $columnName;
	}
}
