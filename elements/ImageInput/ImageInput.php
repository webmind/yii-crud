<?php
/**
 * Created: 15.02.13 14:34
 *
 * @author tcore
 */

class ImageInput extends AbstractInput{

    private $relationName = "";

    private $size = array();

    public static function actions() {
        return array(
            'uploadImage' => 'wmcms.components.editor.elements.imageInput.actions.UploadImageAction',
            'deleteImage' => 'wmcms.components.editor.elements.imageInput.actions.DeleteImageAction',
        );
    }

    /**
     * Создает єлемент для конкретного языка, если он указан
     * @param strung|null $language
     * @param array $params
     * @return string
     */
    public function createInput($language = null, $params = array()) {
        $this->registerAssets();

        return $this->render('image', array(
                'fieldName' => $this->getModelFieldName(),
                'imageList' => $this->getImages(),
                'size' => serialize($this->size),
                'imageGroupId' => $this->getModel()->image_group_id,
                'colors' => CHtml::dropDownList('', 0, $this->colorList(), array(
                            'style' => 'display:none;',
                            'id' => 'image_color_list',
                        )),
            ), true);
    }

    public function input() {
        $area = $this->createInput();
        return $this->render('openHide', array(
                'area' => $area
            ), true);
    }

    private function colorList() {
        $list = CatalogColor::model()->findAll();
        $colors = array();
        foreach ($list AS $v) {
            $colors[$v->id] = $v->name_ru;
        }
        return $colors;
    }

    /**
     * Формируем массив подключенных изображений
     * @param array $imagesArr
     * @return array
     */
    private function getImages($imagesArr=array()) {
        if (is_null($this->getModel()->{$this->getRelationName()})) {
            return array();
        }
        foreach ($this->getModel()->{$this->getRelationName()}->images as $v) {
            $imagePath = Yii::getPathOfAlias('webroot') . DIRECTORY_SEPARATOR .
                ImageUpload::getImagesFolder() . DIRECTORY_SEPARATOR . $v->group_id . DIRECTORY_SEPARATOR . $v->name;
            $image = array();
            $alt = array();
            $title = array();
            if (!file_exists($imagePath)) {
                $image['error'] = Yii::t('imageInput','File not found');
            } else {
                $imageSize = getimagesize($imagePath);
                $image = array(
                    'result' => true,
                    'fileSize' => filesize($imagePath),
                    'imageWidth' => $imageSize[0],
                    'imageHeight' => $imageSize[1],
                );
                $languageList = explode(',', Yii::app()->params['languages']);
                foreach ($languageList AS $language) {
                    $alt['alt_' . $language] = $v->{'alt_' . $language};
                    $title['title_' . $language] = $v->{'title_' . $language};
                }
            }
            array_push($imagesArr, array_merge($image, array(
                        'imageGroupId' => $v->group_id,
                        'imageId' => $v->id,
                        'fileName' => $v->name,
                        'alt' => $alt,
                        'title' => $title,
                        'uploadFolderName' => $this->getModel()->tableName(),
                        'imageFolder' => ImageUpload::getImagesFolder(),
                        'itemImage'=>$v->is_item_image,
                        'priority'=>$v->is_main,
                        'color_id'=> $v->catalog_color_id,
                    )));
        }
        return $imagesArr;
    }

    public function save() {
        $columnName = $this->getModel()->getActiveRelation($this->getRelationName())->foreignKey;
        $this->getModel()->$columnName =
            $_POST['editor'][$this->getRelationName()] == 0 ? null : $_POST['editor'][$this->getRelationName()];
        if (isset($_POST['_imageInput'])) {
            foreach ($_POST['_imageInput'] AS $imageId => $imageData) {
                $image = Image::model()->findByPk($imageId);
                if ($image === null) {
                    continue;
                }
                $image->saveAttributes($imageData);
                $image->is_main =
                    isset($_POST['_imageInputMainImg']) && $_POST['_imageInputMainImg'] == $imageId ? 1 : 0;
                $image->is_item_image = isset($imageData['is_item_image']) ? $imageData['is_item_image'] : 0;
                $image->catalog_color_id = isset($imageData['catalog_color_id']) ? $imageData['catalog_color_id'] : null;
                $image->save();
            }
        }
        return $this->getModel();
    }

    public function setRelationName($relationName) {
        $this->relationName = $relationName;
    }

    public function getRelationName() {
        return $this->relationName;
    }

    public function setSize($size) {
        $this->size = $size;
    }

    private function registerAssets(){
        $cs = Yii::app()->getClientScript();
        $imageInputAssets = Yii::app()->assetManager->publish(
            Yii::getPathOfAlias('yii-crud.elements.ImageInput.assets'), true, -1, YII_DEBUG
        );
        $cs->registerCssFile($imageInputAssets.'/css/imageInput.css');
        $cs->registerScriptFile($imageInputAssets.'/js/imageInput.js');

        $jqueryPlugins = Yii::app()->assetManager->publish(
            Yii::getPathOfAlias('yii-crud.assets.ext'), true, -1, YII_DEBUG
        );
        $cs->registerScriptFile($jqueryPlugins . '/colorbox/jquery.colorbox-min.js');
        $cs->registerCssFile($jqueryPlugins . '/colorbox/colorbox.css');
        $cs->registerScriptFile($jqueryPlugins . '/jquery.form.js');
    }
}