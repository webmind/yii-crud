<input type="hidden" id="imageGroupId" name="editor[<?= $fieldName ?>]" value="<?= $imageGroupId ?>" />
<input type="hidden" id="imagesSize" value='<?= $size ?>' />
<input type="hidden" id="languageList" value="<?= Yii::app()->params['languages'] ?>" />
<input type="hidden" id="siteUrl" value="<?= Yii::app()->getParams()->itemAt('siteUrl') ?>" />
<div id="imageInputBody"></div>
<?= $colors ?>
<script>
	$(document).ready(function() {
		$('#imageInputBody').imageInput('<?= EditorHelper::jsonEncode($imageList) ?>');
	});
</script>