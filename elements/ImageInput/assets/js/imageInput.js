jQuery.fn.imageInput = function ($imageArr){
    if($(this).length != 1){
        console.log('[e] count html objects != 1');
        return;
    }
    var $imageInput = new imageInput();
    $imageInput.create($(this), jQuery.parseJSON($imageArr));
};


function imageInput(){
    this.thead = [{
        'name': '&nbsp;'
    },{
        'name': '&nbsp;'
    },{
        'name': t('imageInput', 'Name')
    },{
        'name': t('imageInput', 'Color')
    },{
        'name': t('imageInput', 'Alternative text')
    },{
        'name': t('imageInput', 'Image title')
    },{
        'name': t('imageInput', 'File size')
    },{
        'name': t('imageInput', 'Image size')
    },{
        'name': '&nbsp;'
    },{
        'name': '&nbsp;'
    },{
        'name': '&nbsp;'
    }];
    this.table = undefined;
}

imageInput.prototype.create = function ($object, $imageArr) {
    $object.addClass('imageInput');
    this.table = this.createImageTable();
    for (var i in $imageArr) {
        this.addImage($imageArr[i], $imageArr[i]['imageGroupId']);
    }
    $object.append(this.table);
    $object.append(this.createAddButton());
    this.createForm();
}

imageInput.prototype.createAddButton = function (){
    var thisClass = this;
    var $div = $('<div></div>')
        .addClass('ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only').click(function (){
            var $wnd = createWindow('imageUploadWindow', {
                title: t('imageInput', 'Image upload'),
                width: 600,
                buttons: [{
                    text: t('imageInput', 'Add image'),
                    id: 'submit_add_image_btn',
                    click: function (){
                        $(this).find('form').submit();
                        // После нажатия на кнопку загрузки изображения делаем ее неактивной
                        $('#submit_add_image_btn').attr('disabled', 'disabled');
                    }
                }]
            },true);
            $wnd.html(thisClass.createEditArea())
            thisClass.createForm();
        });
    var $span = $('<span></span>').addClass('ui-button-text').html(t('imageInput','Add image'));
    $div.append($span);
    return $div;
}

/**
 * Создает форму для редактирования изображений
 * @return {*|jQuery|HTMLElement}
 */
imageInput.prototype.createEditArea = function () {
    //Первый tr
    var $tr1 = $('<tr />').append(
            $('<td />').html(t('imageInput', 'Select file' + ':'))
        ).append(
            $('<td />').append($('<input />').attr({
                type: 'file',
                name: 'files[]',
                min: 1,
                max: 10,
                multiple: true
            }))
        );

    // Второй tr
    var $tr2 = $('<tr />').append(
        $('<td />').html(t('imageInput', 'Alt') + ':'));

    // Для альтов
    var $td2 = $('<td></td>');
    // Список языков системы
    var langs = $('#languageList').val().split(',');
    var isFirst = 'flag_selected';
    // Создаем кнопки переключения языков
    for(var key in langs) {
        $td2.append($('<div />').attr({
            class: 'flag16 flag16' + langs[key] + ' ' + isFirst
        }).html(langs[key]));
        isFirst = '';
    }

    $td2.append($('<div></div>').attr({style: 'clear: both;height: 2px;'}));

    isFirst = '';
    // Создаем инпуты альтов
    for(var key in langs) {
        $td2.append($('<div />').attr({
            class: 'input input_' + langs[key],
            style: isFirst
        }).html($('<input />').attr({
                name: 'alt_' + langs[key],
                type: 'text',
                value: $('#editor_name_' + langs[key]).val()
            })));
        isFirst = 'display: none;';
    }
    $tr2.append($td2);

    // Третий tr
    var $tr3 = $('<tr />').append(
        $('<td />').html(t('imageInput', 'Title') + ':'));

    // Для альтов
    var $td2 = $('<td></td>');
    // Список языков системы
    var langs = $('#languageList').val().split(',');
    var isFirst = 'flag_selected';
    // Создаем кнопки переключения языков
    for(var key in langs) {
        $td2.append($('<div />').attr({
            class: 'flag16 flag16' + langs[key] + ' ' + isFirst
        }).html(langs[key]));
        isFirst = '';
    }

    $td2.append($('<div></div>').attr({style: 'clear: both;height: 2px;'}));

    isFirst = '';
    // Создаем инпуты альтов
    for(var key in langs) {
        $td2.append($('<div />').attr({
            class: 'input input_' + langs[key],
            style: isFirst
        }).html($('<input />').attr({
                name: 'title_' + langs[key],
                type: 'text',
                value: $('#editor_name_' + langs[key]).val()
            })));
        isFirst = 'display: none;';
    }
    $tr3.append($td2);

    //Таблица для рамещения элементов
    var $table = $('<table />').attr('class', 'imageForm').append($tr1).append($tr2).append($tr3);
    //Форма
    var $form = $('<form />').attr({
        action: '/?r=default/editor.ImageInput-UploadImage',
        method: 'post',
        enctype: 'multipart/form-data'
    }).append($table);
    return $form;
}

imageInput.prototype.addImage = function ($image, imageGroupId){
    var $tbody = this.table.find('tbody');
    var $tr = $('<tr></tr>');
    $tr.append(this.createPriority($image));
    $tr.append(this.createItemImage($image));
    $tr.append(this.createName($image));
    $tr.append(this.createColor($image));
    $tr.append(this.createAlt($image));
    $tr.append(this.createTitle($image));
    if ('error' in $image) {
        $tr.append(this.createImageError($image));
    } else {
        $tr.append(this.createFileSize($image));
        $tr.append(this.createImageSize($image));
    }
    $tr.append(this.createImageLink($image, imageGroupId));
    $tr.append(this.createImagePreview($image, imageGroupId));
    $tr.append(this.createImageDelete($image));
    $tbody.append($tr);
    if ($('#imageGroupId').val() == '' || $('#imageGroupId').val() == 0) {
        $('#imageGroupId').val(imageGroupId);
    }
}

/**
 * Вешаем Ajax на форму отправки изображения
 */
imageInput.prototype.createForm = function (){
    var $t = this;
    $('#imageUploadWindow form').ajaxForm({
        dataType: 'json',
        data: {
            folderName: $('#imageFolderName').val(),
            size: $('#imagesSize').val()
        },
        error: function() {
            // Делаем кнопку добавления изображения активной
            $('#submit_add_image_btn').attr('disabled', false);

            createErrorWindow(Array(t('imageInput','Unknown error')));
        },
        beforeSubmit: function (formData, jqForm, options) {
            //alert($('#imageGroupId').val());
            formData.push({ name: 'imageGroupId', value: $('#imageGroupId').val()});
            return true;
        },
        success: function(data) {
            // Делаем кнопку добавления изображения активной
            $('#submit_add_image_btn').attr('disabled', false);

            if('errors' in data){
                createErrorWindow(data.errors);
            }else if('result' in data && data.result == true){
                for (var i in data['images']) {
                    $t.addImage(data['images'][i], data.imageGroupId);
                }
                var $div = createWindow('imageUploadOk', {
                    height: 100,
                    width: 400,
                    title: t('imageInput','The picture has been uploaded'),
                    buttons: [{
                        text: t('imageInput','Upload more'),
                        click: function() {
                            $('#imageUploadWindow input[type="file"]').val('');
                            $(this).dialog( "close" );
                        }
                    },{
                        text: t('imageInput','Close'),
                        click: function() {
                            $(this).dialog('close');
                            $('#imageUploadWindow input[type="file"]').val('');
                            $('#imageUploadWindow').dialog('close');
                        }
                    }]
                });
                $div.html(t('imageInput','The picture has been uploaded successfully, download more?')).attr('style', 'min-height: 20px;');
            }else{
                createErrorWindow(Array(t('imageInput','Unknown error')));
            }
        }
    });
}

imageInput.prototype.createItemImage = function ($image){
    var $td = $('<td></td>');
    var $input = $('<input />').attr('type','checkbox').val(1).
        attr('name','_imageInput[' + $image.imageId + '][is_item_image]');
    if($image.itemImage == 1){
        $input.attr('checked', 'checked');
    }
    $td.append($input);
    return $td;
}

imageInput.prototype.createPriority = function ($image){
    var $td = $('<td></td>');
    var $input = $('<input />').attr('type','radio').val($image.imageId).attr('name','_imageInputMainImg');
    if($image.priority == 1){
        $input.attr('checked', 'checked');
    }
    $td.append($input);
    return $td;
}

imageInput.prototype.createName = function ($image){
    var $td = $('<td></td>').text($image.fileName).addClass('imageName');
    return $td;
}

imageInput.prototype.createColor = function ($image){
    var $td = $('<td></td>').html(
            $('#image_color_list').clone().css('display', '')
                .val($image.color_id).attr('name', '_imageInput[' + $image.imageId  + '][catalog_color_id]')
        )
        .addClass('imageName');
    return $td;
}

/**
 * Создает поля для редактирования alt в таблице изображений
 * @param $image
 * @return {*|jQuery}
 */
imageInput.prototype.createAlt = function ($image){
    var $td = $('<td></td>').addClass('imageAlt');

    // Список языков системы
    var langs = $('#languageList').val().split(',');
    var isFirst = 'flag_selected';
    // Создаем кнопки переключения языков
    for(var key in langs) {
        $td.append($('<div />').attr({
            class: 'flag16 flag16' + langs[key] + ' ' + isFirst
        }).html(langs[key]));
        isFirst = '';
    }

    $td.append($('<div></div>').attr({style: 'clear: both;height: 2px;'}));

    isFirst = '';
    // Создаем инпуты альтов
    for(var key in langs) {
        $td.append($('<div />').attr({
            class: 'input input_' + langs[key],
            style: isFirst
        }).html($('<input />').attr({
                name: '_imageInput[' + $image.imageId + '][alt_' + langs[key] + ']',
                type: 'text',
                value: $image['alt']['alt_' + langs[key]]
            })));
        isFirst = 'display: none;';
    }
    return $td;
}

/**
 * Создает поля для редактирования title в таблице изображений
 * @param $image
 * @return {*|jQuery}
 */
imageInput.prototype.createTitle = function ($image){
    var $td = $('<td></td>').addClass('imageTitle');

    // Список языков системы
    var langs = $('#languageList').val().split(',');
    var isFirst = 'flag_selected';
    // Создаем кнопки переключения языков
    for(var key in langs) {
        $td.append($('<div />').attr({
            class: 'flag16 flag16' + langs[key] + ' ' + isFirst
        }).html(langs[key]));
        isFirst = '';
    }

    $td.append($('<div></div>').attr({style: 'clear: both;height: 2px;'}));

    isFirst = '';
    // Создаем инпуты альтов
    for(var key in langs) {
        $td.append($('<div />').attr({
            class: 'input input_' + langs[key],
            style: isFirst
        }).html($('<input />').attr({
                name: '_imageInput[' + $image.imageId + '][title_' + langs[key] + ']',
                type: 'text',
                value: $image['title']['title_' + langs[key]]
            })));
        isFirst = 'display: none;';
    }
    return $td;
}

imageInput.prototype.createFileSize = function ($image){
    var $td = $('<td></td>').addClass('imageFileSize').text(Math.round($image.fileSize/1024/1024*1000)/1000 + 'Mb.');
    return $td;
}

imageInput.prototype.createImageSize = function ($image){
    var $td = $('<td></td>').addClass('imageSize').text($image.imageWidth + 'x' + $image.imageHeight);
    return $td;
}

imageInput.prototype.createImageError = function ($image){
    var $td = $('<td></td>').addClass('imageError').text($image.error).attr('colspan', '2');
    return $td;
}

imageInput.prototype.createImageLink = function ($image, imageGroupId){
    var $td = $('<td></td>').addClass('imageLink');
    var $a = $('<a></a>').attr('href','#').click(function (){
        prompt(
            t('imageInput', 'Use Ctrl + C, Enter to copy to the clipboard'),
            $('#siteUrl').val() + '/userfiles/' + imageGroupId + '/' + $image.fileName
        );
    });
    $td.append($a);
    return $td;
}

imageInput.prototype.createImageDelete = function ($image){
    var $td = $('<td></td>').addClass('imageDelete');
    var $t = this;
    var $a = $('<a></a>').attr('href','#').click(function (){
        $t.deleteImage($image, $td);
    });
    $td.append($a);
    return $td;
}

imageInput.prototype.createImagePreview = function ($image, imageGroupId){
    var $td = $('<td></td>').addClass('imagePreview');
    var $href = $('#siteUrl').val() + '/userfiles/' + imageGroupId + '/' + $image.fileName;
    var $a = $('<a></a>').attr('target','_blank').attr('href',$href).click(function (){
        $.colorbox({
            href: $href,
            maxWidth: '100%',
            maxHeight: '100%'
        });
        return false;
    });
    $td.append($a);
    return $td;
}

/**
 * Удаление картинки
 * @param $image
 * @param $td
 * @return {boolean}
 */
imageInput.prototype.deleteImage = function ($image, $td){
    $this = $(this);
    var $div = createWindow('deleteImageConfirm', {
        height: 100,
        width: 400,
        title: t('imageInput','Deleting images'),
        buttons: [{
            text: t('imageInput','Delete'),
            click: function() {
                $.ajax({
                    url: '/?r=default/editor.ImageInput-DeleteImage',
                    dataType: 'json',
                    data: {
                        id: $image.imageId
                    },
                    beforeSend: function(){
                        $td.parent().css('display','none');
                    },
                    error: function() {
                        createErrorWindow(Array(t('imageInput','Unknown error')));
                        $td.parent().css('display','');
                    },
                    success: function (data){
                        if('errors' in data){
                            createErrorWindow(data.errors);
                            $td.parent().css('display','');
                        }else if('result' in data && data.result == true){
                            $(this).parent().parent().remove();
                        }else{
                            createErrorWindow(Array(t('imageInput','Unknown error')));
                            $td.parent().css('display','');
                        }
                    }
                });
                $(this).dialog("close");
            }
        },{
            text: t('imageInput','Cancel'),
            click: function() {
                $(this).dialog("close");
            }
        }]
    });
    $div.html(t('imageInput','The image will be deleted, continue?')).attr('style', 'min-height:20px;');
    return false;
}

imageInput.prototype.createImageTable  = function (){
    var $table = $('<table></table>');
    var $thead = $('<thead></thead>');
    var $tbody = $('<tbody></tbody>');
    var $tr = $('<tr></tr>');
    for(var $i=0;$i<this.thead.length;$i++){
        var $th = $('<th></th>').html(this.thead[$i].name);
        if('css' in this.thead[$i]){
            $th.css(this.thead[$i].css);
        }
        if('style' in this.thead[$i]){
            $th.css(this.thead[$i].style);
        }
        $tr.append($th);
    }
    $thead.append($tr);
    $table.append($thead);
    $table.append($tbody);
    return $table;
}