<?php
/**
 * Created: 15.02.13 17:11
 * 
 * @author tcore
 */
 
class UploadImageAction extends CAction{

	public function run() {
		Yii::import('yii-crud.elements.ImageInput.ImageUpload');
		$imageUpload = new ImageUpload();
		echo $imageUpload->upload();
	}
}
