<?php
/**
 * Created: 22.02.13 22:48
 * 
 * @author tcore
 */
 
class DeleteImageAction extends CAction{

	public function run() {
		Yii::import('yii-crud.elements.ImageInput.ImageUpload');
		$imageUpload = new ImageUpload();

		echo $imageUpload->deleteImage($_GET['id']);
	}
}
