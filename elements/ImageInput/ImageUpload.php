<?php

class ImageUpload {
    /**
     * @var int - максимально допустимый размер файла в байтах (3mb)
     */
    private $_maxFileSize = 3145728;

    /**
     * @var array - масив разрешенных расширений для файлов, которые будут загружены
     */
    private $_allowExtensions = array('jpg', 'png', 'gif', 'jpeg');

    /**
     * @var array - масив разрешенных типов файлов, которые будут загружены
     */
    private $_allowMimeType = array('image/jpeg', 'image/gif', 'image/png');

    /**
     * @var int - идентификатор групы изображений
     */
    private $_imageGroupId;

    /**
     * Массив переданных файлов
     * @var CUploadedFile[]
     */
    private $_files;

    /**
     * @var array - альтернативный текст для изображения, передаеться пользователем через $_POST
     */
    private $_alt;

    /**
     * @var array - title для изображения, передаеться пользователем через $_POST
     */
    private $_title;

    /**
     * @var array - список размеров изображения которые нужно создать
     */
    private $size;

    public function upload() {
        Yii::import('yii-crud.components.EditorHelper');
        $result = $this->getFiles();
        if ($result !== true) {
            return EditorHelper::jsonEncode(array('errors'=>$result));
        }
        $transaction = Yii::app()->db->beginTransaction();
        $files = array();
        try {
            foreach ($this->_files AS $i => $file) {
                $imageSize = getimagesize($file->getTempName());
                $image = new Image();
                if ($this->_imageGroupId == 0) {
                    $group = $this->createGroup();
                    $image->is_main  = 1;
                } else {
                    $group = ImageGroup::model()->findByPk($this->_imageGroupId);
                    if (count($group->images) > 0) {
                        $images = $group->images;
                        $colorId = $images[0]->catalog_color_id;
                        $image->catalog_color_id = $colorId;
                    }
                }
                $image->group_id = $this->_imageGroupId;
                $image->name = $file->getName();
                $image->is_item_image = 1;
                $mainAlt = array();
                foreach ($this->_alt AS $cellName => $alt) {
                    $mainAlt[$cellName] = $alt . ' ' . (count($group->images) + 1);
                    $image->$cellName = $alt . ' ' . (count($group->images) + 1);
                }

                $mainTitle = array();
                foreach ($this->_title AS $cellName => $title) {
                    $mainTitle[$cellName] = $title . ' ' . (count($group->images) + 1);
                    $image->$cellName = $title . ' ' . (count($group->images) + 1);
                }

                if ( ! $image->save()) {
                    $errors = array_map(function($item){
                            return implode(',,', $item);
                        }, $image->getErrors());
                    throw new CException(implode(',,',$errors));
                }

                if ( ! $this->saveFile($file)) {
                    throw new CException('Ошибка при сохраении файлов');
                }

                $files[$i] = array(
                    'imageId' => $image->id,
                    'itemImage' => $image->is_item_image,
                    'fileName' => $file->getName(),
                    'alt' => $mainAlt,
                    'title' => $mainTitle,
                    'fileSize' => $file->getSize(),
                    'imageWidth' => $imageSize[0],
                    'imageHeight' => $imageSize[1],
                    'priority' => $image->is_main,
                    'color_id' => $image->catalog_color_id,
                );
            }



//			$imageSize = getimagesize($this->_file->getTempName());
//			$image = new Image();
//			if($this->_imageGroupId==0){
//				$this->createGroup();
//			}
//			$image->group_id = $this->_imageGroupId;
//			$image->name = $this->_file->getName();
//
//			foreach ($this->_alt AS $cellName => $alt) {
//				$image->$cellName = $alt;
//			}
//
//			foreach ($this->_title AS $cellName => $title) {
//				$image->$cellName = $title;
//			}
//			if(!$image->save()){
//				$errors = array_map(function($item){
//					return implode(',,', $item);
//				}, $image->getErrors());
//				throw new Exception(implode(',,',$errors));
//			}
//			if(!$this->saveFile($this->_file)){
//				throw new Exception($this->_file->getError());
//			}
            $transaction->commit();
        } catch(Exception $e) {
            $transaction->rollback();
            return EditorHelper::jsonEncode(array('errors'=>explode(',,',$e->getMessage())));
        }
        return EditorHelper::jsonEncode(array(
                'result' => true,
                'imageGroupId' => $this->_imageGroupId,
                'imageFolder' => $this->getImagesFolder(),
                'images' => $files,
//			'imageId'=>$image->id,
//			'itemImage'=>$image->is_item_image,
//			'fileName'=>$this->_file->getName(),
//			'alt'=>$this->_alt,
//			'title'=>$this->_title,
//			'fileSize'=>$this->_file->getSize(),
//			'imageWidth'=>$imageSize[0],
//			'imageHeight'=>$imageSize[1],
//			'priority'=>0,
            ));
    }

    private function createGroup() {
        $imageGroup = new ImageGroup();
        $imageGroup->save();
        $this->_imageGroupId = $imageGroup->id;

        return $imageGroup;
    }

    /**
     * Сохраняет файл на диске, проверяет наличие папок на диске в случае их отсутствия создает их
     * Создает миниатюры для загружаемого файла
     * @param CUploadedFile $file
     * @return bool
     * @throws CException
     */
    private function saveFile($file) {
        // Папка для хранения папок групп изображений
        $uploadFolder = Yii::getPathOfAlias('webroot') . DIRECTORY_SEPARATOR . $this->getImagesFolder();
        if ( ! file_exists($uploadFolder)) {
            mkdir($uploadFolder,0755);
        }
        // Папка группы изображений
        $uploadFolder .= DIRECTORY_SEPARATOR . $this->_imageGroupId;
        if ( ! file_exists($uploadFolder)) {
            mkdir($uploadFolder,0755);
        }

        if (file_exists($uploadFolder . DIRECTORY_SEPARATOR . $file->getName())) {
            throw new CException("Уже существует файл с таким именем");
        }

        if ($file->saveAs($uploadFolder . DIRECTORY_SEPARATOR . $file->getName())) {
            $image = WideImage::load($uploadFolder . DIRECTORY_SEPARATOR . $file->getName());
            // Создаем миниатюры для изображения
            foreach ($this->size as $k => $v) {
                if (isset($v['width']) || isset($v['height'])) {
                    // Определяем высоту миниатюры
                    $height = $image->getHeight();
                    if (isset($v['height'])) {
                        $height = $v['height'];
                    }
                    // Определяем ширину миниатюры
                    $width = $image->getHeight();
                    if (isset($v['width'])) {
                        $width = $v['width'];
                    }
                    // Определяем митод трансформации
                    if ( ! isset($v['fit']) || ! in_array($v['fit'], array('inside','outside','fill'))) {
                        $v['fit'] = 'inside';
                    }
                    $newImage = $image->resize($width, $height, $v['fit']);
                    $newUploadFolder = $uploadFolder . DIRECTORY_SEPARATOR . $k;
                    if ( ! file_exists($newUploadFolder)) {
                        mkdir($newUploadFolder, 0777);
                    }
                    $newImage->saveToFile($newUploadFolder . DIRECTORY_SEPARATOR . $file->getName());
                }
            }
            return true;
        } else {
            return false;
        }
    }

    /**
     * Проверяет входные данные на прпвильность возвращат масив ошибок, в случае неудачи. или true в случае успеха
     * @return array | bool
     */
    private function getFiles() {
        $error = array();
        // Проверка передан ли Id группы
        if ( ! isset($_POST['imageGroupId'])) {
            $error[] = Yii::t('imageInput', 'Unknown group id');
        } else {
            $this->_imageGroupId = (int) $_POST['imageGroupId'];
        }
        $languages = explode(',', Yii::app()->params['languages']);

        // Проверка переданы ли alt на всех языках
        foreach ($languages AS $language) {
            if( ! isset($_POST['alt_' . $language])){
                $error[] = Yii::t('imageInput','Alternative text {langName} is not specified', array(
                        '{langName}' => strtoupper($language)));
            }else{
                $this->_alt['alt_' . $language] = $_POST['alt_' . $language];
            }
        }

        // Проверка переданы ли title на всех языках
        foreach ($languages AS $language) {
            if( ! isset($_POST['title_' . $language])){
                $error[] = Yii::t('imageInput','Image title {langName} is not specified', array(
                        '{langName}' => strtoupper($language)));
            }else{
                $this->_title['title_' . $language] = $_POST['title_' . $language];
            }
        }

        if ( ! isset($_POST['size'])) {
            $error[] = Yii::t('imageInput','Size is not specified');
            return $error;
        }
        $this->size = unserialize($_POST['size']);
        if ($this->size === false) {
            $error[] = Yii::t('imageInput','Size is not serialized');
            return $error;
        }

        $this->_files = CUploadedFile::getInstancesByName('files');
        if ($this->_files === null) {
            $error[] = Yii::t('imageInput','The file was not transferred');
            return $error;
        }

        foreach ($this->_files AS $file) {
            if ($file->getSize() > $this->_maxFileSize) {
                $error[] = Yii::t(
                    'imageInput',
                    'File "{fileName}" too large, the maximum size - {maxFileSize}Mb.',
                    array(
                        '{maxFileSize}' => $this->_maxFileSize/1024/1024,
                        '{fileName}' => $file->getName(),
                    )
                );
            }

            if (array_search($file->getExtensionName(), $this->_allowExtensions) === false) {
                $error[] = Yii::t(
                    'imageInput',
                    'Invalid extension "{extension}" in file "{fileName}", use the {allowExtensions}',
                    array(
                        '{extension}' => $file->getExtensionName(),
                        '{allowExtensions}' => implode(', ', $this->_allowExtensions),
                        '{fileName}' => $file->getName(),
                    )
                );
            }

            $mimeType = CFileHelper::getMimeType($file->getTempName());
            if (array_search($mimeType, $this->_allowMimeType) === false) {
                $error[] = Yii::t(
                    'imageInput',
                    'Invalid mime type "{mimeType}" in file {fileName}, use the {allowMimeType}',
                    array(
                        '{mimeType}' => $mimeType,
                        '{allowMimeType}' => implode(', ',$this->_allowMimeType),
                        '{fileName}' => $file->getName(),
                    )
                );
            }
        }

        return count($error) == 0 ? true : $error;
    }

    /**
     * Папка в которой будут храниться файлы
     * @static
     * @return string
     */
    public static function getImagesFolder() {
        return '../../www/public/userfiles';
    }

    /**
     * Удаляет группы изображений которые созданы больше двух дней назад и до сих пор с меткой is_tmp = 1
     * Удаляет изображения которые не связаны ни с одной группой
     * @static
     */
    public static function deleteOld() {
        $oldDate = (new DateTime())->modify('-0 day')->format('Y-m-d');

        $groupList = ImageGroup::model()->findAll(array(
                'condition' => 'date_add <= :oldDate AND is_tmp = 1',
                'params' => array(
                    ':oldDate' => $oldDate,
                ),
            ));
        foreach ($groupList AS $group) {
            self::deleteImageGroup($group->id);
            echo "[i] deleting image group $group->id\n";
        }


        $imageList = Image::model()->with('imageGroup')->findAll(array('condition' => 'imageGroup.id IS NULL'));
        foreach ($imageList as $image) {
            $groupId = $image->imageGroup->id;
            echo "[i] deleting img id: $image->id, group: $groupId\n";
            self::deleteImage($image->id);
        }

        // Удаляем картирнки не связанные с БД
        $path = Yii::getPathOfAlias('application') . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'www' .
            DIRECTORY_SEPARATOR . self::getImagesFolder() . DIRECTORY_SEPARATOR;
        $dh = opendir($path);
        while (false !== ($dir = readdir($dh))) {
            if (is_dir($path . $dir) && $dir !== '.' && $dir !== '..') {
                if ( ImageGroup::model()->findByPk($dir) == null) {
                    ImageUpload::deleteImageGroup($dir);
                    echo "[i] delete group $dir from disk\n";
                }
            } else {
                continue;
            }
        }
        closedir($dh);
    }

    /**
     * Удаляет изображение по id
     * @param $id
     * @return string
     * @throws Exception
     */
    public static function deleteImage($id) {
        $image = new Image();
        Yii::import('yii-crud.components.EditorHelper');
        try{
            $image = $image->findByPk($id);

            $path = Yii::getPathOfAlias('webroot') . DIRECTORY_SEPARATOR . self::getImagesFolder() .
                DIRECTORY_SEPARATOR . $image->group_id . DIRECTORY_SEPARATOR;

            if(file_exists($path . $image->name)){
                unlink($path . $image->name);
            }
            if(is_dir($path)){
                $dh = opendir($path);
                while (false !== ($dir = readdir($dh))) {
                    if (is_dir($path . $dir) && $dir !== '.' && $dir !== '..') {
                        if(file_exists($path . $dir . DIRECTORY_SEPARATOR . $image->name)){
                            unlink($path . $dir . DIRECTORY_SEPARATOR . $image->name);
                        }
                    } else {
                        continue;
                    }
                }
                closedir($dh);
            }
            $image->delete();
            return EditorHelper::jsonEncode(array('result'=>true));
        }catch(Exception $e){
            return EditorHelper::jsonEncode(array('errors'=>array($e->getMessage())));
        }
    }

    /**
     * Удалет группу изображений
     * @param $id
     */
    public static function deleteImageGroup($id) {
        /**
         * @var ImageGroup $group
         */
        $group = ImageGroup::model()->findByPk($id);
        if ( ! is_null($group)) {
            foreach ($group->images AS $image) {
                self::deleteImage($image->id);
            }
        }
        $group->delete();
    }

    /**
     * Удаляет изображение и все кго экземпляры во вложенных папках
     * @param $folderName
     * @param $groupId
     * @param $imageName
     */
    private static function deleteImageFromDisk($folderName, $groupId, $imageName) {
        Yii::import('yii-crud.components.EditorHelper');
        // Путь к группе изображений
        $path = Yii::getPathOfAlias('webroot') . DIRECTORY_SEPARATOR . self::getImagesFolder() . DIRECTORY_SEPARATOR .
            $folderName . DIRECTORY_SEPARATOR . $groupId . DIRECTORY_SEPARATOR;

        // Удаляем главное изображение
        if(file_exists($path . $imageName)){
            unlink($path . $imageName);
        }
        // Удаляем все экземпляры картинки во вложеных папках
        if(is_dir($path)){
            $dh = opendir($path);
            while (false !== ($dir = readdir($dh))) {
                if (is_dir($path . $dir) && $dir !== '.' && $dir !== '..') {
                    if(file_exists($path . $dir . DIRECTORY_SEPARATOR . $imageName)){
                        unlink($path . $dir . DIRECTORY_SEPARATOR . $imageName);
                    }
                } else {
                    continue;
                }
            }
            closedir($dh);
        }
    }

    /**
     * Удаление изображения с базы данных по идентификатору
     * @param $imageId
     * @return bool
     */
    private function deleteImgFromDB($imageId) {
        $image = Image::model()->findByPk($imageId);

        if ( ! is_null($image)) {
            return $image->delete();
        }
        return true;
    }
}
