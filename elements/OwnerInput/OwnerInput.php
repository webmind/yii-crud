<?php
/**
 * Created: 24.02.13 18:51
 * 
 * @author tcore
 */
 
class OwnerInput extends AbstractInput{

	private $columnName = 'owner_id';

	/**
	 * Создает єлемент для конкретного языка, если он указан
	 * @param strung|null $language
	 * @param array $params
	 * @return string
	 */
	public function createInput($language = null, $params = array()) {
		return CHtml::tag('div', array(), Yii::app()->user->getName());
	}

	public function save() {
		$_POST['editor'][$this->getColumnName()] = (int) Yii::app()->user->getId();
		return parent::save();
	}

	public function setColumnName($columnName) {
		$this->columnName = $columnName;
	}

	public function getColumnName() {
		return $this->columnName;
	}
}
