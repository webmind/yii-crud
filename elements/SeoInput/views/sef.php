<?
/**
 * @var Seo $model
 * @var string $sef
 */
?>
<table>
	<tr>
<!--		<td style="width: 10%">--><?//= CHtml::activeLabel($model, 'sef') ?><!--:</td>-->
		<td style="width: 10%">ЧПУ:</td>
<!--		<td>-->
<!--			--><?//= CHtml::activeTextField($model, 'sef', array(
//				'name' => 'editor[seo][sef]'
//			)) ?>
<!--		</td>-->
		<td>
            <div class="generate_sef"></div>
			<?= CHtml::textField('editor[seo][sef]', $sef, array(
                'style' => 'width: 90%;',
            )) ?>
		</td>
	</tr>
	<tr>
		<td style="width: 10%"><?= CHtml::activeLabel($model, 'canonical') ?>:</td>
		<td><?= CHtml::activeTextField($model, 'canonical', array(
				'name' => 'editor[seo][canonical]'
			)) ?></td>
	</tr>
</table>