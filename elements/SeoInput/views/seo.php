<?
$title = 'title' . $languageSuffix;
$keywords = 'keywords' . $languageSuffix;
$description = 'description' . $languageSuffix;
$relationName = $this->getRelationName();
?>
<table>
	<tr>
		<td style="width: 10%"><?= CHtml::activeLabel($seo, $title) ?>:</td>
		<td>
            <div class="generate_seo"></div>
            <?= CHtml::activeTextField($seo, $title, array(
                'name' => 'editor[seo][' . $title . ']',
                'style' => 'width: 90%',
            )) ?>
        </td>
	</tr>
	<tr>
		<td><?= CHtml::activeLabel($seo, $description) ?>:</td>
		<td>
            <div class="generate_seo"></div>
            <?= CHtml::activeTextField($seo, $description, array(
                'name' => 'editor[seo][' . $description . ']',
                'style' => 'width: 90%',
            )) ?>
        </td>
	</tr>
	<tr>

		<td><?= CHtml::activeLabel($seo, $keywords) ?>:</td>
		<td>
            <div class="generate_seo"></div>
            <?= CHtml::activeTextField($seo, $keywords, array(
                'name' => 'editor[seo][' . $keywords . ']',
                'style' => 'width: 90%',
            )) ?>
        </td>
	</tr>
</table>