<?
/**
 * @var string $area
 */
?>
<div class="openHideEditArea"><?= Yii::t('editor', 'open | hide') ?></div>
<div class="editArea" style="display: none;"><?= $area ?></div>