<?php
/**
 * Created: 05.02.13 21:40
 * 
 * @author tcore
 */
 
class SeoInput extends AbstractInput{

	/**
	 * @var string
	 */
	private $relationName;

	/**
	 * Создание элемента
	 * @param null $language
	 * @param array $params
	 * @return string
	 */
	public function createInput($language = null, $params = array()) {
		$this->registerAssets();
		$relationName = $this->getRelationName();
		if ($language !== null) {
			$languageSuffix = '_' . $language;
		} else {
			$languageSuffix = '';
		}
		return $this->render('seo',
			array(
				'seo' => $this->getModel()->$relationName,
				'languageSuffix' => $languageSuffix
			),
			true
		);
	}

	/**
	 * Переопределяем родительски метод создания элемента для того чтобы внизу добавить поле ЧПУ
	 * @return string
	 */
	public function input() {
		$relationName = $this->getRelationName();
		if (is_null($this->getModel()->$relationName)) {
			$this->getModel()->$relationName = new Seo();
		}
		$sef = '';
		if ($this->getModel()->$relationName != null) {
			$seoSefs = $this->getModel()->$relationName->seoSef;
			foreach ($seoSefs AS $seoSef) {
				if ($seoSef->is_main == 1) {
					$sef = $seoSef->sef;
					break;
				}
			}
		}
		$sefHtml = $this->render('sef', array(
			'model' => $this->getModel()->$relationName,
			'sef' => $sef,
		), true);
		$area = parent::input() . $sefHtml;
		return $this->render('openHide', array(
			'area' => $area
		), true);
	}

	/**
	 * @return CActiveRecord
	 */
	public function save() {
		$relationName = $this->getRelationName();
		if (is_null($this->getModel()->$relationName)) {
			$this->getModel()->$relationName = new Seo();
		}
		if ($this->getMultilingual()) {
			$languages = explode(',', Yii::app()->params['languages']);
			foreach($languages as $v) {
				$this->saveOne('title_'.$v, 'keywords_'.$v, 'description_'.$v, 'canonical');
			}
		} else {
			$this->saveOne('title', 'keywords', 'description', 'canonical');
		}
		$saveResult = $this->getModel()->$relationName->save();
		$columnName = $this->getModel()->getActiveRelation($this->getRelationName())->foreignKey;
		$this->getModel()->$columnName = $this->getModel()->$relationName->id;
		if ( ! $saveResult) {
			Yii::app()->messageUtil->add($this->getModel()->$relationName->getErrors(), 'error');
			// Добавляем выделение ошибки в заглавной label элемента
			$this->getModel()->addError(
				$this->getColumnName(),
				Yii::t(
					'editor', 'You must fill all the parameters specified in the {attributeLabel}',
					array(
						'{attributeLabel}' => $this->getModel()->getAttributeLabel($this->getColumnName())
					)
				)
			);
		}
		return $this->getModel();
	}

	public function afterSave() {
		if ( ! $this->saveSef()) {
			$this->getModel()->addError(
				$this->getColumnName(),
				Yii::t(
					'editor', 'You must fill all the parameters specified in the {attributeLabel}',
					array(
						'{attributeLabel}' => $this->getModel()->getAttributeLabel($this->getColumnName())
					)
				)
			);
			throw new CException('Ошибка в процессе сохранения ЧПУ');
		}
	}

	/**
	 * Сохранение ЧПУ которое ввел пользователь
	 * Если такой ЧПУ еще не добавлен, то добавляем его, указываем его главным,
	 * все остальные ЧПУ указываем как вторичные
	 * Если такой ЧПУ уже существует, проверяем опрделенный ли он как главный,
	 * если нет то определяем его главный, все оствльные ЧПУ указываем как вторичные
	 * @return bool
	 */
	protected function saveSef() {
		$seoSef = SeoSef::model()->find(array(
			'condition' => 't.sef LIKE :sef AND t.seo_id = :seo_id',
			'params' => array(
				':sef' => $_POST['editor']['seo']['sef'],
				':seo_id' => $this->getModel()->seo->id,
			),
		));
		if ($seoSef === null) {
			if ( ! $this->clearMainSef() ) {
				return false;
			}
			$seoSef = new SeoSef();
			$seoSef->sef = $_POST['editor']['seo']['sef'];
			$seoSef->is_main = 1;
			$seoSef->seo_id = $this->getModel()->seo->id;
			if ( ! $seoSef->save()) {
				Yii::app()->messageUtil->add($seoSef->getErrors(), 'error');
			} else {
				return true;
			}
		} else {
			if ($seoSef->is_main !== 1) {
				if ( ! $this->clearMainSef() ) {
					return false;
				}
				$seoSef->is_main = 1;
				return $seoSef->save();
			} else {
				return true;
			}
		}
	}

	/**
	 * Делает все ЧПУ вторичными
	 * @return boolean
	 */
	protected function clearMainSef() {
		$sefs = $this->getModel()->seo->seoSef;
		$result = true;
		foreach ($sefs AS $sef) {
			if ($sef->is_main == 1) {
				$sef->is_main = 0;
				if ( ! $sef->save() ) {
					$result = false;
				}
			}
		}
		return $result;
	}

	/**
	 * @param string $titleColName
	 * @param string $keywordsColName
	 * @param string $descriptionColName
	 * @param $canonicalColName
	 */
	public function saveOne($titleColName, $keywordsColName, $descriptionColName, $canonicalColName) {
		$relationName = $this->getRelationName();
		$this->getModel()->$relationName->$titleColName = $_POST['editor']['seo'][$titleColName];
		$this->getModel()->$relationName->$keywordsColName = $_POST['editor']['seo'][$keywordsColName];
		$this->getModel()->$relationName->$descriptionColName = $_POST['editor']['seo'][$descriptionColName];
		$this->getModel()->$relationName->$canonicalColName = $_POST['editor']['seo'][$canonicalColName];
	}

	private function registerAssets(){
		Yii::app()->getClientScript()->registerScriptFile(
			Yii::app()->assetManager->publish(
				Yii::getPathOfAlias('yii-crud'), false, -1, YII_DEBUG
			) . '/assets/js/jquery.synctranslit.min.js'
		);
	}

	/**
	 * @param string $relationName
	 */
	public function setRelationName($relationName) {
		$this->relationName = $relationName;
	}

	/**
	 * @return string
	 */
	public function getRelationName() {
		return $this->relationName;
	}

	/**
	 * @return string
	 */
	public function getColumnName() {
		return $this->getRelationName();
	}
}
