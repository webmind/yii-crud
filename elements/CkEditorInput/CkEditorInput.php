<?php
/**
 * Created: 02.02.13 13:24
 * 
 * @author tcore
 */
 
class CkEditorInput extends AbstractInput{

	/**
	 * @var string имя столбца в БД
	 */
	private $columnName;

	public function createInput($language = null, $params=array()) {
		$this->registerAssets();
		if ($language != null) {
			$columnName = $this->getColumnName() . '_' . $language;
		} else {
			$columnName = $this->getColumnName();
		}
		$params = array_merge(array(
			'name' => 'editor[' . $columnName . ']',
			'class' => 'ckeditor',
		));
		return CHtml::activeTextArea($this->getModel(), $columnName, $params);
	}

	public function input() {
		$area = parent::input();
		return $this->render('openHide', array(
			'area' => $area
		), true);
	}

	/**
	 * @return string
	 */
	public function getColumnName() {
		return $this->columnName;
	}

	/**
	 * @param string $columnName
	 */
	public function setColumnName($columnName) {
		$this->columnName = $columnName;
	}

	private function registerAssets(){
		$cs = Yii::app()->getClientScript();
		$imageInputAssets = Yii::app()->assetManager->publish(
			Yii::getPathOfAlias('yii-crud.assets.ext.ckeditor'), true, -1, YII_DEBUG
		);
		$cs->registerScriptFile($imageInputAssets.'/ckeditor.js');
	}
}
