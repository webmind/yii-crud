<?
/**
 * @var array $colNames
 * @var array $colModel
 * @var array $data
 * @var string $relationName
 */
?>

<div id="spt_<?= $relationName ?>">Показать/скрыть</div>
<table id="param_table_<?= $relationName ?>" style="display: none;"></table>
<script type="text/javascript">
$(document).ready(function () {
	var colNames = $.parseJSON('<?= str_replace('\'', '\\\'', json_encode($colNames)) ?>');
	var relationName = '<?= $relationName ?>';
	var data = $.parseJSON('<?= str_replace('\'', '\\\'', json_encode($data)) ?>');
	var colModel = $.parseJSON('<?= str_replace('\'', '\\\'', json_encode($colModel, JSON_UNESCAPED_UNICODE)) ?>');

	createParamInputTable('param_table_' + relationName, colNames, data, colModel, relationName);
	$("#spt_" + relationName).button().click(function () {
		if ($("#param_table_" + relationName).css('display') == 'none') {
			$("#param_table_" + relationName).css('display', '');
		} else {
			$("#param_table_" + relationName).css('display', 'none');
		}
	});
});
</script>