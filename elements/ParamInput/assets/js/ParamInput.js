function createParamInputTable (elementId, colNames, data, colModel, relationName) {
	createParamInputTableHeader(elementId, colNames);

	for(var i = 0; i < data.length; i++) {
		addRow(elementId, data[i], colModel, relationName);
	}

	createAddRowBtn(elementId, colModel, relationName);
}

function createAddRowBtn(elementId, colModel, relationName) {
	var $button = $('<div />').text('Add').button().click(function () {
		addRow(elementId, '', colModel, relationName);
	});
	$("#" + elementId).append(
		$('<tfoot />').append(
			$('<tr />').append(
				$('<td />').attr('colspan', colModel.length).css('text-align', 'right').append($button)
			)));
}

function createParamInputTableHeader(elementId, colNames) {
	var $tr = $('<tr />');
	for(var i = 0; i < colNames.length; i++) {
		var $th = $('<th />').html(colNames[i]);
		$tr.append($th);
	}
	$tr.append($('<th />').html('&nbsp;').css('width', '1px'));
	$("#" + elementId).append($('<thead />').append($tr));
}

function addRow(elementId, rowData, colModel, relationName) {
	var $tr = $('<tr />');
	for(var i = 0; i < colModel.length; i++) {
		var $td = $('<td />');

		if (colModel[i]['type'] == 'text') {
			var $inp = $('<input />').
				attr('type', 'text').
				val(rowData[i]).
				attr('name', 'editor[' + relationName + '][' + $("#" + elementId + " tr").length + '][' + colModel[i]['name'] + ']');
		}

		if (colModel[i]['type'] == 'select') {
			$td.css('width', '1px');
			var $inp = $('<select></select>').
				attr('name', 'editor[' + relationName + '][' + $("#" + elementId + " tr").length + '][' + colModel[i]['name'] + ']');
			for (var id in colModel[i]['items']) {
				$inp.append($("<option></option>")
					.attr("value", id)
					.text(colModel[i]['items'][id]));
			}
			$inp.val(rowData[i]);
		}

		if (colModel[i]['type'] == 'hidden') {
			$td.css('width', '1px');
			var $inp = $('<input />').
				attr('type', 'hidden').
				val(rowData[i]).
				attr('name', 'editor[' + relationName + '][' + $("#" + elementId + " tr").length + '][' + colModel[i]['name'] + ']');
		}

		$td.append($inp);
		if (colModel[i]['type'] == 'select') {
			$inp.combobox();
		}
		$tr.append($td);
	}
	$tr.append(
		$('<td />').append(
			$('<div />').text('Delete').button().click(function () {
				$(this).parent().parent().remove();
			})
		)
	);
	$("#" + elementId).append($tr);
}