<?php

/**
 * Редактирование строк свзяанной таблици, поддреживает такие типы полей: hidden, text, select
 * Если указать в настройках столюца visible => false столбец не будет выводится
 * Eсли указать в настройках столюца значение value => '<?php_code?>' то результат выполнения кода будет записан в ячейку
 * Пример конфугкрирования:
 * array(
 *  'type' => 'param',
 *  'config' => array(
 *    'relationName' => 'catalogPrices',
 *      'columns' => array(
 *          array(
 *              'name' => 'id',
 *              'type' => 'hidden',
 *          ),
 *          array(
 *              'name' => 'price',
 *              'type' => 'text',
 *          ),
 *          array(
 *              'relationName' => 'catalogPriceParam',
 *              'relationView' => 'name',
 *              'type' => 'select',
 *          ),
 *          array(
 *              'name' => 'catalog_category_id',
 *              'value' => '$this->getModel()->id',
 *              'visible' => false,
 *          ),
 *      ),
 *  ),
 * )
 */
class ParamInput extends AbstractInput{

	/**
	 * Название отношения в настройках редактируемой таблици
	 * @var string
	 */
	private $relationName;

	/**
	 * Конфигурация вывода столбцов
	 * @var array
	 */
	private $columns;

	/**
	 * @var YiiCrudActiveRecord|null
	 */
	private $relatedModel = null;

	/**
	 * Создает єлемент
	 * @param string|null $language
	 * @param array $params
	 * @return string
	 */
	public function createInput($language = null, $params = array()) {
		$this->registerAssets();

		$this->render('param', array(
			'relationName' => $this->relationName,
			'data' => $this->createData(),
			'colNames' => $this->createColNames(),
			'colModel' => $this->createColModel(),
		));
	}

	public function save() {
		return $this->getModel();
	}

	/**
	 * Сохранеине данных, выволяняется после сохранения основной модели
	 * Сначала удаляются все связанные строки, заново создаются и добавляются в базу строки
	 * Если в конфиге передано значение value то это сторока будет выолнена с помощью eval и результат ее
	 * вывполнения будет записан в БД
	 * @return CActiveRecord|void
	 */
	public function afterSave() {
		$modelName = $this->getModel()->relations()[$this->relationName][1];
		// Название ячейки в которой храниться id основной модели
		$modelRelationColumn = $this->getModel()->relations()[$this->relationName][2];

		// Удаление всех связанных строк
		foreach ($this->getModel()->{$this->relationName} AS $relatedItem) {
			if ( ! $relatedItem->delete() ) {
				Yii::app()->messageUtil->add($relatedItem->getErrors(), 'error', false);
			}
		}

		if (isset($_POST['editor'][$this->relationName])) {
			foreach ($_POST['editor'][$this->relationName] AS $v) {
				$item = new $modelName;

				foreach ($this->columns AS $column) {
					if (isset($column['value'])) {
						$item->$column['name'] = eval('return ' . $column['value'] . ';');
						continue;
					}

					if ($column['type'] == 'select') {
						// Определяем имя поля куда записывать значение выпадающего списка
						$relationColumn = $item->relations()[$column['relationName']][2];
						$item->{$relationColumn} = $v[$column['relationName']];
					} else if ($column['type'] == 'text' || $column['type'] == 'hidden') {
						$item->{$column['name']} = $v[$column['name']];
					}
				}
				$item->$modelRelationColumn = $this->getModel()->id;
				if ( ! $item->save() ){
					Yii::app()->messageUtil->add($item->getErrors(), 'error', false);
				}
			}
		}
	}

	/**
	 * Создает массив названий столбцов
	 * Если паармет visible для столбца установлен как false то столбец выведен не будет
	 * Если type установлен как hidden то в ячейку запишем пустое значение
	 * @return array
	 */
	private function createColNames() {
		$attributeLabels = $this->relatedModel->attributeLabels();
		$colNames = array();
		foreach($this->getColumns() AS $column) {
			if ( ! (isset($column['visible']) && $column['visible'] === false)) {
				if ($column['type'] == 'hidden') {
					$colNames[] = '';
				} else if ($column['type'] == 'text') {
					if (isset($attributeLabels[$column['name']])) {
						$colNames[] = $attributeLabels[$column['name']];
					} else {
						$colNames[] = $column['name'];
					}
				} else if ($column['type'] == 'select') {
					if (isset($attributeLabels[$column['relationName']])) {
						$colNames[] = $attributeLabels[$column['relationName']];
					} else {
						$colNames[] = $column['relationName'];
					}
				}
			}
		}
		return $colNames;
	}

	/**
	 * Создание массива с данными для вывода строк
	 * Если паармет visible для столбца установлен как false то столбец выведен не будет
	 * Если type установлен как select то в ячейку запишем id связанной по relationName таблице
	 * Если type установлен как text или hidden то в ячейку запишем значения поля $name
	 * @return array
	 */
	private function createData() {
		$lines = array();
		foreach ($this->getModel()->{$this->relationName} AS $v) {
			$row = array();
			foreach($this->getColumns() AS $column) {
				if ( ! (isset($column['visible']) && $column['visible'] === false)) {
					if ($column['type'] == 'select') {
						if ($v->{$column['relationName']} !== null) {
							$row[] = $v->{$column['relationName']}->id;
						} else {
							$row[] = null;
						}
					} else if ($column['type'] == 'text' || $column['type'] == 'hidden') {
						$row[] = $v->{$column['name']};
					}
				}
			}
			$lines[] = $row;
		}
		return $lines;
	}

	/**
	 * Создание модели таблици
	 * @return array
	 */
	private function createColModel() {
		$colModel = array();
		foreach($this->getColumns() AS $column) {
			if ( ! (isset($column['visible']) && $column['visible'] === false)) {
				if ($column['type'] == 'select') {
					$model = $this->relatedModel->relations()[$column['relationName']][1];
					$model = new $model;
					$items = $model->findAll();
					$itemsArr = array();
					foreach ($items AS $item) {
						$itemsArr[$item->id] = $item->{$column['relationView']};
					}
					$colModel[] = array('type' => 'select', 'name' => $column['relationName'], 'items' => $itemsArr);
				} else if ($column['type'] == 'text' || $column['type'] == 'hidden') {
					$colModel[] = array('type' => $column['type'], 'name' => $column['name']);
				}
			}
		}
		return $colModel;
	}

	protected function registerAssets() {
		$cs = Yii::app()->getClientScript();
		$assets = Yii::app()->assetManager->publish(
			Yii::getPathOfAlias('yii-crud.elements.ParamInput.assets'), true, -1, YII_DEBUG
		);
		$cs->registerCssFile($assets.'/css/jquery.combobox.css');
		$cs->registerScriptFile($assets . '/js/jquery.combobox.js');
		$cs->registerScriptFile($assets.'/js/ParamInput.js');
	}

	/**
	 * @return string
	 */
	public function getRelationName() {
		return $this->relationName;
	}

	/**
	 * @param string $relationName
	 */
	public function setRelationName($relationName) {
		$this->relationName = $relationName;
		$this->relatedModel = $this->getModel()->relations()[$this->getRelationName()][1];
		$this->relatedModel = new $this->relatedModel;
	}

	/**
	 * @return array
	 */
	public function getColumns() {
		return $this->columns;
	}

	/**
	 * @param array $columns
	 */
	public function setColumns($columns) {
		$this->columns = $columns;
	}
}