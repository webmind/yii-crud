<?php
/**
 * Created: 28.02.13 14:09
 * 
 * @author tcore
 */
 
class PriceInput extends AbstractInput{

	private $relationName;

	private $inputName = '_price';

	public function setRelationName($relationName) {
		$this->relationName = $relationName;
	}

	public function getRelationName() {
		return $this->relationName;
	}

	public function createInput($language = null, $params = array()) {
		$this->registerAssets();
		$currencies = Currency::model()->findAll();
		$currencyArr = array();
		foreach($currencies AS $currency) {
			$currencyArr[] = array('name' => $currency->short_name, 'id' => $currency->id);
		}

		$priceParams = CatalogPriceParam::model()->findAll();
		$priceParamArr = array();
		foreach($priceParams AS $priceParam) {
			$priceParamArr[] = array('name' => $priceParam->name, 'id' => $priceParam->id);
		}

		$priceArr = array();
		if ($this->getModel()->{$this->getRelationName()} !== null) {
			$prices = $this->getModel()->{$this->getRelationName()}->catalogPrices;
			foreach($prices AS $price) {
				$priceArr[$price->id] = array(
					'id' => $price->id,
					'catalog_price_param_id' => $price->catalog_price_param_id,
					'price' => $price->price,
					'currencyId' => $price->currency_id,
				);
			}
		}

		if (isset($_POST[$this->inputName]) && is_array($_POST[$this->inputName]) &&
			Yii::app()->messageUtil->getMessageCount('error') > 0) {
			$priceArr = array();
			foreach ($_POST[$this->inputName] AS $id => $values) {
				$priceArr[$id] = array(
					'id' => $id,
					'catalog_price_param_id' => $values['catalog_price_param_id'],
					'price' => $values['price'],
					'currencyId' => $values['currency_id'],
				);
			}
		}

		return $this->render('list', array(
			'currencies' => EditorHelper::jsonEncode($currencyArr),
			'priceParams' => EditorHelper::jsonEncode($priceParamArr),
			'prices' => EditorHelper::jsonEncode($priceArr),
			'inputName' => $this->inputName,
		));
	}

	public function setMultilingual($isMultilingual) {
		Yii::app()->messageUtil->add(
			Yii::t('editor', "{elementName} can't be multilingual", array('{elementName}' => 'PriceInput')),
			'warning'
		);
		parent::setMultilingual(false);
	}

	public function registerAssets() {
		$assets = Yii::app()->assetManager->publish(
			Yii::getPathOfAlias('yii-crud.elements.PriceInput.assets.*'), false, -1, YII_DEBUG
		);
		Yii::app()->getClientScript()->registerCssFile($assets . '/css/PriceInput.css');
		Yii::app()->getClientScript()->registerScriptFile($assets . '/js/PriceInput.js');
	}

	public function save() {
		if ( ! isset($_POST[$this->inputName])) {
			$this->getModel()->{$this->getRelationName()} = null;
			return $this->getModel();
		}

		$priceGroup = $this->getModel()->{$this->getRelationName()};
		if ($this->getModel()->{$this->getRelationName()} == null) {
			$priceGroup = new CatalogPriceGroup();
			if ( ! $priceGroup->save() ) {
				Yii::app()->messageUtil->add($priceGroup->getErrors(), 'error', false);
			}
		}

		if ($this->getModel()->{$this->getRelationName()} !== null) {
			foreach ($this->getModel()->{$this->getRelationName()}->catalogPrices AS $catPrice) {
				if ( ! $catPrice->delete() ) {
					Yii::app()->messageUtil->add($catPrice->getErrors(), 'error', false);
				}
			}
		}

		$price = $_POST[$this->inputName];
		foreach($price AS $id => $values) {
			$catalogPrice = new CatalogPrice();
			$catalogPrice->setAttributes($price[$id]);
			$catalogPrice->group_id = $priceGroup->id;
			if (substr($id, 0, 3) != 'new') {
				$catalogPrice->id = $id;
			}
			if ( ! $catalogPrice->save() ){
				Yii::app()->messageUtil->add($catalogPrice->getErrors(), 'error', false);
			}
		}
		$this->getModel()->catalog_price_group_id = $priceGroup->id;
		return $this->getModel();
	}
}
