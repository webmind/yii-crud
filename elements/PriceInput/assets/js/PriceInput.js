jQuery.fn.priceInput = function (price, name){
	price = jQuery.parseJSON(price);
	var elementCounter = 0;
	for(var i in price){
		$(this).append(createOne(price[i]['param'], price[i]['price'], price[i]['currencyId'], name, price[i]['id']));
		if (price[i]['id'].substr(0, 3) == 'new') {
			var num = parseInt(price[i]['id'].substr(3));
			if (num >= elementCounter) {
				elementCounter = num;
				elementCounter++;
			}
		}
	}

	$('.add_price_element_button').click(function() {
		$('.priceInput table table tbody').append(createOne('', '', '', name, 'new' + elementCounter));
		elementCounter++;
	});

	function createOne(param, price, currencyId, name, id) {
		var $tr = $('<tr />').append(
				$('<td />').append(
						createParamInput(param, name, id)
					).attr({
						class: 'param'
					})
			).append(
				$('<td />').append(
						createPriceInput(price, name, id)
					).attr({
						class: 'price'
					})
			).append(
				$('<td />').append(
						createCurrencySelect(currencyId, name, id)
					).attr({
						class: 'currency'
					})
			).append(
				$('<td />').append(
						createDeleteButton()
					)
			);
		return $tr;
	}

	function createParamInput(param, name, id) {
		return $('<input />').attr({
			type: 'text',
			value: param,
			name: name + '[' + id + '][param]'
		});
	}

	function createPriceInput(price, name, id) {
		return $('<input />').attr({
			type: 'text',
			value: price,
			name: name + '[' + id + '][price]'
		});
	}

	function createDeleteButton() {
		return $('<div />').attr({class: 'button',style: 'font-size: 18px;'}).html('&ndash;').click(function () {
			$(this).parent().parent().remove();
		});
	}

	/**
	 * Создание элемента для выбора валюты
	 * @param selectedId
	 * @return {*|jQuery|HTMLElement}
	 * @param name
	 * @param id
	 */
	function createCurrencySelect(selectedId, name, id) {
		var $select = $('<select />');
		var currencies = jQuery.parseJSON($('.currencies').html());
		for (var i = 0; i < currencies.length; i++) {
			var $option = $('<option />').attr({value: currencies[i].id}).html(currencies[i].name);
			$select.append($option);
		}
		$select.val(selectedId);
		$select.attr('name', name + '[' + id + '][currency_id]');
		return $select;
	}


	/**
	 * Создание элемента для выбора параметра
	 * @param selectedId
	 * @return {*|jQuery|HTMLElement}
	 * @param name
	 * @param id
	 */
	function createCurrencySelect(selectedId, name, id) {
		var $select = $('<select />');
		var currencies = jQuery.parseJSON($('.currencies').html());
		for (var i = 0; i < currencies.length; i++) {
			var $option = $('<option />').attr({value: currencies[i].id}).html(currencies[i].name);
			$select.append($option);
		}
		$select.val(selectedId);
		$select.attr('name', name + '[' + id + '][currency_id]');
		return $select;
	}
};

