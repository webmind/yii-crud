<div class="priceInput">
	<table cellpadding="0">
		<tr>
			<td>
				<table>
					<tr>
						<th><?= CatalogPrice::model()->getAttributeLabel('param') ?></th>
						<th><?= CatalogPrice::model()->getAttributeLabel('price') ?></th>
						<th><?= CatalogPrice::model()->getAttributeLabel('currency_id') ?></th>
						<th>&nbsp;</th>
					</tr>
				</table>
			</td>
			<td style="vertical-align: bottom;padding: 0 0 11px 0;">
				<div class="button add_price_element_button"><?= Yii::t('system', 'Add') ?></div>
			</td>
		</tr>
	</table>
</div>
<div class="currencies" style="display: none;"><?= $currencies ?></div>
<script type="text/javascript">
	$('.priceInput table table tbody').priceInput('<?= $prices ?>', '<?= $inputName ?>');
</script>