<?php
/**
 * Created: 05.02.13 1:30
 * 
 * @author tcore
 */
 
class SelectInput extends AbstractInput{

	/**
	 * @var string
	 */
	private $relationName;

	/**
	 * @var string
	 */
	private $relationView;

	/**
	 * @var array
	 */
	private $listData = array();

	/**
	 * @var array
	 */
	private $listSelectedItems = array();

	public function save() {
		$relationName = $this->getRelationName();
		// Если пользователь хочет записать в поле значени null
		if (isset($_POST['editor']['is_null'][$this->getModelFieldName()])
			&& $_POST['editor']['is_null'][$this->getModelFieldName()] == 1
			&& $this->getAllowNull()) {
			$a = $this->getModel()->relations()[$relationName];
			if ($this->getModel()->relations()[$relationName][0] == CActiveRecord::BELONGS_TO) {
				$this->getModel()->{$this->getModel()->relations()[$relationName][2]} = null;
			}
			$this->getModel()->$relationName = null;
			return $this->getModel();
		}
		$selectIdList = array();
		if (isset($_POST['editor'][$relationName]) && is_array($_POST['editor'][$relationName])) {
			$selectIdList = $_POST['editor'][$relationName];
		}
		$relation = $this->getModel()->getActiveRelation($this->getRelationName());
		if (get_class($relation) == 'CBelongsToRelation' &&
			isset($_POST['editor'][$relationName]) &&  count($_POST['editor'][$relationName]) == 1) {
			if (is_string($_POST['editor'][$relationName])) {
				$this->getModel()->{$relation->foreignKey} = $_POST['editor'][$relationName];
			} else if (is_array($_POST['editor'][$relationName])) {
				$this->getModel()->{$relation->foreignKey} = $_POST['editor'][$relationName][0];
			}
		}
		$this->getModel()->$relationName = $selectIdList;
		return $this->getModel();
	}

	public function createInput($language = null, $params = array()) {
		//TODO: Многоязычность
		$this->createListData();
		$this->createSelectedItemsList();
		return $this->render('select', array(
			'columnName' => $this->getRelationName(),
			'listData' => $this->listData,
			'selectedItemsList' => $this->listSelectedItems,
			'multiple' => ($this->getModel()->getActiveRelation($this->getRelationName()) instanceof CManyManyRelation),
		), true);
	}

	public function createListData() {
		$relationObject = $this->getRelationObject();
		$isParent = false;
		if (array_search('parent_id', $relationObject::model()->tableSchema->columnNames)) {
			$isParent = true;
		}
		$this->createList($relationObject::model()->findAll(), 0, 0, $isParent);
	}

	public function createSelectedItemsList() {
		$relationName = $this->getRelationName();
		$selectedItemsList = $this->getModel()->$relationName;
		if (is_array($selectedItemsList)) {
			foreach ($selectedItemsList AS $v) {
				if (is_numeric($v)) {
					$this->listSelectedItems[] = $v;
				} elseif (is_object($v)) {
					$this->listSelectedItems[] = $v->id;
				}
			}
		} else if ($selectedItemsList != null) {
			$this->listSelectedItems[] = $selectedItemsList->id;
		}
	}

	public function createList($objList, $parent_id=0, $lvl=0, $isParent=false) {
		$prefix = '';
		for ($i = 0; $i < $lvl; $i++) {
			$prefix .= '--';
		}
		foreach ($objList as $k => $obj) {
			if ($isParent) {
				if ($obj->parent_id == $parent_id) {
					unset($objList[$k]);
					$this->listData[$obj->id] = $prefix . $obj->{$this->getRelationView()};
					$this->createList($objList, $obj->id, $lvl + 1, $isParent);
				}
			} else {
				$this->listData[$obj->id] = $prefix . $obj->{$this->getRelationView()};
			}
		}
	}

	/**
	 * @param string $relationName
	 */
	public function setRelationName($relationName) {
		$this->relationName = $relationName;
	}

	/**
	 * @return string
	 */
	public function getRelationName() {
		return $this->relationName;
	}

	/**
	 * @param string $relationView
	 */
	public function setRelationView($relationView) {
		$this->relationView = $relationView;
	}

	/**
	 * @return string
	 */
	public function getRelationView() {
		return $this->relationView;
	}

	/**
	 * @return string
	 */
	public function getRelationObject() {
		return $this->getModel()->getActiveRelation($this->getRelationName())->className;
	}

	/**
	 * @return string
	 */
	public function getColumnName() {
		return $this->getRelationName();
	}
}
