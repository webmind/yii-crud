<?php
/**
 * Created: 05.02.13 22:37
 * 
 * @author tcore
 */
 
class DateInput extends AbstractInput{

	/**
	 * @var string имя столбца в БД
	 */
	private $columnName;

	public function createInput($language = null, $params = array()) {
		$columnName = $this->getColumnName();
		return $this->render('date', array(
				'name' => 'editor[' . $columnName . ']',
				'value' => $this->getModel()->$columnName,
			), true);
	}

	public function save() {
		$columnName = $this->getColumnName();
		try{
			$date = new DateTime($_POST['editor'][$columnName]);
			$this->getModel()->$columnName = $date->format('Y-m-d');
		} catch (Exception $e) {
			$this->getModel()->addError(
				$this->getColumnName(),
				Yii::t('editor', 'Failure to get date')
			);
		}
		return $this->getModel();
	}

	/**
	 * @return string
	 */
	public function getColumnName() {
		return $this->columnName;
	}

	/**
	 * @param string $columnName
	 */
	public function setColumnName($columnName)
	{
		$this->columnName = $columnName;
	}
}
