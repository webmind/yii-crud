<?
$date = new DateTime($value);
$this->widget('zii.widgets.jui.CJuiDatePicker',array(
	'name'=>$name,
	'value'=>$date->format('d.m.Y'),
	'model'=>$this->getModel(),
	'options'=>array(
		'showAnim'=>false,
		'dateFormat'=>'dd.mm.yy',
	),
	'htmlOptions'=>array(
		'name'=>'editor[' . $this->getColumnName() . ']'
	),
));
