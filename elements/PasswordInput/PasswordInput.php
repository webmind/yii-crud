<?php
/**
 * Created: 01.02.13 15:34
 * 
 * @author tcore
 */

class PasswordInput extends AbstractInput{

	/**
	 * @var string имя столбца в БД
	 */
	private $columnName;

	public function createInput($language = null, $params=array()) {
		$params_first = array_merge(array(
			'name' => 'editor[' . $this->getColumnName() . '_1]',
			'value' => '',
		), $params);
		$params_second = array_merge(array(
			'name' => 'editor[' . $this->getColumnName() . '_2]',
			'value' => '',
		), $params);
		return $this->render('password', array(
			'columnName' => $this->getColumnName(),
			'params_first' => $params_first,
			'params_second' => $params_second,
		), true);
	}

	/**
	 * @return string
	 */
	public function getColumnName() {
		return $this->columnName;
	}

	/**
	 * @param string $columnName
	 */
	public function setColumnName($columnName) {
		$this->columnName = $columnName;
	}

	public function save() {
		if (isset($_POST['editor'][$this->getColumnName() . '_1']) &&
			isset($_POST['editor'][$this->getColumnName() . '_2']) &&
			$_POST['editor'][$this->getColumnName() . '_2'] == '') {
			return $this->getModel();
		} else if (isset($_POST['editor'][$this->getColumnName() . '_1']) &&
			isset($_POST['editor'][$this->getColumnName() . '_2']) &&
			$_POST['editor'][$this->getColumnName() . '_2'] == $_POST['editor'][$this->getColumnName() . '_1']) {
			$_POST['editor'][$this->getColumnName()] =
                $this->getModel()->encryptPassword($_POST['editor'][$this->getColumnName() . '_1']);
			return parent::save();
		} else {
			Yii::app()->messageUtil->add('Error while saving data for password input', 'error');
			return $this->getModel();
		}
	}
}
