<?php
/**
 * Поддерживает свойства columnName
 */
class TextInput extends AbstractInput{

	/**
	 * Имя поля в модели данных
	 * @var string
	 */
	private $columnName;

	/**
	 * Выражение, результат которого запишется в поле, если оно пустое
	 * @var bool|string
	 */
	private $defaultValueFunc = false;

	public function createInput($language = null, $params=array()) {
		if ($language != null) {
			$columnName = $this->getColumnName() . '_' . $language;
		} else {
			$columnName = $this->getColumnName();
		}
		$params = array_merge(array('name' => 'editor[' . $columnName . ']'));

		if ($this->getModel()->$columnName == '' && $this->defaultValueFunc !== false) {
			$call = $this->defaultValueFunc;
			$this->getModel()->$columnName = $call($this);
		}

		return CHtml::activeTextField($this->getModel(), $columnName, $params);
	}

	/**
	 * @return string
	 */
	public function getColumnName() {
		return $this->columnName;
	}

	/**
	 * @param string $columnName
	 */
	public function setColumnName($columnName) {
		$this->columnName = $columnName;
	}

	/**
	 * @return bool|string
	 */
	public function getDefaultValueFunc() {
		return $this->defaultValueFunc;
	}

	/**
	 * @param bool|string $defaultValueExpression
	 */
	public function setDefaultValueFunc($defaultValueExpression) {
		$this->defaultValueFunc = $defaultValueExpression;
	}
}
