<?php
/**
 * Created: 02.02.13 13:24
 * 
 * @author tcore
 */
 
class AreaInput extends AbstractInput{

	/**
	 * @var string имя столбца в БД
	 */
	private $columnName;

	public function createInput($language = null, $params=array()) {
		if ($language != null) {
			$columnName = $this->getColumnName() . '_' . $language;
		} else {
			$columnName = $this->getColumnName();
		}
		$params = array_merge(array('name' => 'editor[' . $columnName . ']'));
		return CHtml::activeTextArea($this->getModel(), $columnName, $params);
	}

	/**
	 * @return string
	 */
	public function getColumnName() {
		return $this->columnName;
	}

	/**
	 * @param string $columnName
	 */
	public function setColumnName($columnName)
	{
		$this->columnName = $columnName;
	}
}
