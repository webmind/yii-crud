<?php

class IsActiveColumn extends CDataColumn {

	public $filter = array(0 => 'Нет', 1 => 'Да');

	public $htmlOptions = array('style' => 'width:1%;');

	/**
	 * Renders the data cell content.
	 * This method evaluates {@link value} or {@link name} and renders the result.
	 * @param integer $row the row number (zero-based)
	 * @param mixed $data the data associated with the row
	 */
	protected function renderDataCellContent($row,$data) {
		if($this->value!==null)
			$value=$this->evaluateExpression($this->value,array('data'=>$data,'row'=>$row));
		elseif($this->name!==null)
			$value=CHtml::value($data,$this->name);

		if ($value == 1) {
			echo CHtml::image($this->grid->baseAssetsUrl . '/img/active.png', 'Да');
		} else if ($value == 0) {
			echo CHtml::image($this->grid->baseAssetsUrl . '/img/not-active.png', 'Нет');
		} else {
			echo CHtml::image($this->grid->baseAssetsUrl . '/img/vopros.gif', 'Неизвестно');
		}
	}
}