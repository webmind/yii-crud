yii-crud
========

Расширение для Yii фреймворка для работы с CRUD операциями.

Примеры использования:
* Форма редактирования <br />
https://github.com/tcore/WmCMS/blob/master/www/themes/wmcms/views/wmcms/catalog/item/update.php
* Вывод информации в виде Grid
https://github.com/tcore/WmCMS/blob/master/www/themes/wmcms/views/wmcms/catalog/item/list.php
