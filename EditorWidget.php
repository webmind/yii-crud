<?php
/**
 * Created: 01.02.13 15:17
 * 
 * @author tcore
 */

class EditorWidget extends CWidget{

	/**
	 * @var array массив элемнтов для оедактирования
	 */
	public $elements = array();

	/**
	 * @var CActiveRecord
	 */
	public $model = null;

	/**
	 * @var int ID редактируемой записи
	 */
	public $id = null;

	public $onAfterSave = null;

	private function registerAssets(){
		Yii::app()->clientScript->registerCoreScript('jquery');
		Yii::app()->clientScript->registerCoreScript('jquery.ui');

		Yii::app()->clientScript->registerCssFile(
			Yii::app()->clientScript->getCoreScriptUrl() . '/jui/css/base/jquery-ui.css');

		$assets = Yii::app()->assetManager->publish(
			Yii::getPathOfAlias('yii-crud.assets.*'), false, -1, YII_DEBUG
		);
		Yii::app()->getClientScript()->registerCssFile(
			$assets . DIRECTORY_SEPARATOR . 'css' . DIRECTORY_SEPARATOR . 'editor.css'
		);
		Yii::app()->getClientScript()->registerScriptFile(
			$assets . DIRECTORY_SEPARATOR . 'js' . DIRECTORY_SEPARATOR . 'editor.js', CClientScript::POS_BEGIN
		);
	}

	public function init() {
		$row = $this->model->findAllByPk($this->id);
		if (isset($row[0])) {
			$this->model = $row[0];
		}
		Yii::import('yii-crud.components.*');
		$this->registerAssets();
	}

	/**
	 * Создаем HTML формы
	 */
	public function run(){
		// Если методом POST были переданы значения сохраним их
		if (isset($_POST['editor'])) {
			$this->save();
			$this->model->refresh();
		}
		$formElements = array();
		// Перебираем элементы которые передали в конфиге
		foreach ($this->elements as $element) {
			// Название класа который отвечает за тип элемента
			$className = ucfirst($element['type']) . 'Input';
			Yii::import('yii-crud.elements.' . $className . '.*');
			$input = new $className();
			$input->setModel($this->model);
			if (isset($element['config'])) {
				$input->setConfig($element['config']);
			}
			$formElements[] = $input;
		}
		$this->render('form', array('formElements' => $formElements));
	}

	private function save() {
		$this->model->clearErrors();
		if ($_GET['id'] == 0) {
			$this->model->setIsNewRecord(true);
		}
		$transaction = Yii::app()->db->beginTransaction();
		try {
			foreach ($this->elements as $element) {
				$className = ucfirst($element['type']) . 'Input';
				Yii::import('yii-crud.elements.' . $className . '.*');
				$input = new $className();
				$input->setModel($this->model);
				if (isset($element['config'])) {
					$input->setConfig($element['config']);
				}
				$this->model = $input->save();
			}
			$this->addPosition();
			// Валидация модели
			if ( ! $this->model->validate(null, false) ) {
				Yii::app()->messageUtil->add($this->model->getErrors(), 'error');
			}

			// Сохранение модели
			if ( ! $this->model->save(false) ) {
				Yii::app()->messageUtil->add($this->model->getErrors(), 'error');
			}

			// Выполняем методы сохраения после сохранения основной модели
			foreach ($this->elements as $element) {
				$className = ucfirst($element['type']) . 'Input';
				Yii::import('yii-crud.elements.' . $className . '.*');
				$input = new $className();
				$input->setModel($this->model);
				if (isset($element['config'])) {
					$input->setConfig($element['config']);
				}
				$input->afterSave();
			}

			// Если были какието ошибки выкидываем исключение
			if (Yii::app()->messageUtil->getMessageCount('error') > 0) {
				throw new Exception(Yii::t('editor', 'Error while saving data'));
			}

			$transaction->commit();

			if ($this->onAfterSave !== null) {
				// TODO: Выводить ошибку в случае нейдачи
				$onAfterSave = $this->onAfterSave;
				$onAfterSave($this);
			}

			Yii::app()->messageUtil->add('Successfully stored', 'success');
			if ($_GET['id'] == 0) {
				$modelName = get_class($this->model);
				$this->model = new $modelName();
			}
		} catch (Exception $e) {
			$transaction->rollback();
			Yii::app()->messageUtil->add($e->getMessage(), 'error');
		}
	}

	private function addPosition() {
		if(array_search('position', $this->model->tableSchema->columnNames) !== false &&
				$this->model->getIsNewRecord()){
			$criteria = new CDbCriteria();
			$criteria->order = 'position DESC';
			$criteria->select = 'position';
			$max = $this->model->find($criteria);
			$this->model->position = $max['position'] + 1;
		}
	}

	public static function actions() {
		$actions = array();
		$elementsDirPath = dirname(__FILE__) . DIRECTORY_SEPARATOR . 'elements' . DIRECTORY_SEPARATOR;
		$elementsDirHandle = opendir($elementsDirPath);
		// Перебор всех папок в каталоге elements
		while (false !== ($entry = readdir($elementsDirHandle))) {
			if (substr($entry, -5) == 'Input') {
				$actionsDirPth = $elementsDirPath . DIRECTORY_SEPARATOR . $entry . DIRECTORY_SEPARATOR . 'actions' .
					DIRECTORY_SEPARATOR;
				if (file_exists($actionsDirPth) && is_dir($actionsDirPth)) {
					$actionsDirHandle = opendir($actionsDirPth);
					// Если есть папка actions перебираем в ней файлы
					while (false !== ($actionFile = readdir($actionsDirHandle))) {
						if (substr($actionFile, -10) == 'Action.php') {
							// Формируем строку action
							$actions[$entry . '-' . substr($actionFile, 0, -10)] =
								'yii-crud.elements.' . $entry . '.actions.' . substr($actionFile, 0, -4);
						}
					}
					closedir($actionsDirHandle);
				}
			}
		}
		closedir($elementsDirHandle);
		return $actions;
	}
}
