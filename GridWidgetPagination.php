<?php

class GridWidgetPagination extends CPagination {

	protected $pageSizeConfig = array(25 => 25, 50 => 50, 100 => 100, 1 => 1);

	public function __construct($itemCount=0) {
		parent::__construct($itemCount);

		// Установим количество строк на страницу
		$this->pageSize = $this->getPageSize();
	}

	public function setPageSize($value) {
		if (array_search($value, $this->pageSizeConfig) !== false) {
			parent::setPageSize($value);
		}
	}

	/**
	 * Определяет сколько нужно выводить записей на страницу
	 * @return int
	 */
	public function getPageSize() {
		$pageSize = $this->pageSizeConfig[$this->getPageSizeKey()];
		return $pageSize;
	}

	/**
	 * Генерирует HTML код для выбора количества строк выводимых на страницуы
	 * @return string
	 */
	public function renderPageSizeSelect() {
		$html = Yii::t('editor', 'Show on page');
		$options = '';
		foreach ($this->pageSizeConfig AS $k => $v) {
			$url = Yii::app()->createUrl(Yii::app()->getController()->route, array_merge($_GET, array('pageSize' => $k)));
			$htmlOption = array();
			$htmlOption['value'] = $url;
			if ($k == $this->getPageSizeKey()) {
				$htmlOption['selected'] = 'selected';
			}
			$options .= CHtml::tag('option', $htmlOption, $v) . "\n";
		}
		return $html . ': ' . CHtml::tag('select', array('id' => 'countOnPageSelect'), $options);
	}

	/**
	 * @return array
	 */
	public function getPageSizeConfig() {
		return $this->pageSizeConfig;
	}

	/**
	 * @param array $pageSizeConfig
	 */
	public function setPageSizeConfig($pageSizeConfig) {
		$this->pageSizeConfig = $pageSizeConfig;
	}

	/**
	 * Возвращает ключ массива $this->pageSizeConfig
	 * Определяет сколько нужно выводить записей на страницу
	 * Если передан параметр $_GET[pageSize] возвращает $this->pageSizeConfig[$_GET[pageSize]]
	 * если такой элемент массива pageSizeConfig существует
	 * в инных случаях возвращает количество записей на страницу по умолчанию
	 * @return int
	 */
	protected function getPageSizeKey () {
		if (isset($_GET['pageSize']) && isset($this->pageSizeConfig[$_GET['pageSize']])) {
			return $_GET['pageSize'];
		}
		return $this->getDefaultPageSizeKey();
	}

	/**
	 * Возвращает ключ массива $this->pageSizeConfig
	 * Определяет сколько по умолчанию нужно выводить записей на страницу
	 * Берет нулевой элемент массива $this->pageSizeConfig, если его не существует генерирует CException
	 * @throws CException
	 * @return int
	 */
	protected function getDefaultPageSizeKey () {
		if (count($this->pageSizeConfig) > 0) {
			reset($this->pageSizeConfig);
			return key($this->pageSizeConfig);
		}
		throw new CException();
	}
}