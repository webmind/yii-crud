<?php
/**
 * Created: 25.02.13 18:38
 * 
 * @author tcore
 */

Yii::import('zii.widgets.grid.CGridView');
Yii::import('yii-crud.GridWidgetPagination');

class GridWidget extends CGridView{

	/**
	 * Модель с которой будем вытягивать данные
	 * @var BActiveRecord
	 */
	public $model;

	/**
	 * Отключаем ajax подгрузку данных
	 * @var bool
	 */
	public $ajaxUpdate = false;

	/**
	 * Настрока вывода кнопок
	 * @var string
	 */
	public $buttonConfig = '{update} {delete}';

	/**
	 * Критерий выборки строк из таблици
	 * @var CDbCriteria
	 */
	public $criteria = null;

	/**
	 * В случае если модель необходимо выводить иерархично указываем название отношения,
	 * по которому будет идти связка уровней иерархии
	 * @var bool
	 */
	public $parentRelationName = false;

	/**
	 * Путь от корня web сервера к публичной директории
	 * @var string
	 */
	public $baseAssetsUrl;

	/**
	 * Метод возвращает конфиг кнопок
	 * @param string $template - шаблон отображения кнопок
	 * @return array
	 */
	public function getButtonsConfig($template='{update} {delete}'){
		$controllerName = Yii::app()->controller->id;
		$actionName = Yii::app()->controller->action->id;
		return array(
			'class'=>'CButtonColumn',
			'deleteConfirmation'=>"js:'Запись с ID '+$(this).parent().parent().children(':first-child').text()+' будет удалена! Продолжить?'",
			'template'=>$template,
			'buttons'=>array(
				'delete'=>array(
					'options'=>array(
						'title'=>'Удалить',
					),
					'url'=>'Yii::app()->controller->createUrl("'.$controllerName.'/'.$actionName.'",array("delete"=>$data->id))',
				),
				'update'=>array(
					'options'=>array(
						'title'=>'Редактировать',
					),
				),
				'up'=>array(
					'label'=>'&uarr;',
					'options'=>array(
						'title'=>'Вверх',
						'style'=>'text-decoration:none;font-size:18px;color:red;',
					),
					'url'=>'Yii::app()->controller->createUrl("'.$controllerName.'/'.$actionName.'", array("up"=>$data->id))',
				),
				'down'=>array(
					'label'=>'&darr;',
					'options'=>array(
						'title'=>'Вниз',
						'style'=>'text-decoration:none;font-size:18px;color:red;',
					),
					'url'=>'Yii::app()->controller->createUrl("'.$controllerName.'/'.$actionName.'", array("down"=>$data->id))',
				),
			),
			'htmlOptions'=>array(
				'width'=>'1%',
				'nowrap'=>'',
			),
		);
	}

	/**
	 * Метод меняет позицию строки в таблице в зависимости от переданных данных
	 * @return none
	 */
	public function upDownRow(){
		if ( isset($_GET['up']) ) {
			$sql[0] = '<';
			$sql[1] = 'DESC';
			$id = (int)$_GET['up'];
		} else if ( isset($_GET['down']) ) {
			$sql[0] = '>';
			$sql[1] = 'ASC';
			$id = (int)$_GET['down'];
		} else {
			return;
		}
		$cur = $this->model->findByPk($id);
		if ( $cur !== null ) {
			$curPosition = $cur->position;
			$criteria = new CDbCriteria;
			$criteria->select = 'id';
			$criteria->condition = 'position '.$sql[0].' :position';
			$criteria->order = 'position '.$sql[1].'';
			$criteria->params = array(':position' => $curPosition);
			$up = $this->model->find($criteria);
			if ( $up !== null ) {
				$up = $this->model->findByPk($up->id);
				$curPosition = $cur->position;
				$newPos = $up->position;
				$cur->position = $newPos;
				$up->position = $curPosition;
				$transaction = Yii::app()->db->beginTransaction();
				try {
					if ( ! $up->save() ) {
						Yii::app()->messageUtil->add($up->getErrors(), 'error');
					}
					if ( ! $cur->save() ) {
						Yii::app()->messageUtil->add($cur->getErrors(), 'error');
					}
					$transaction->commit();
					Yii::app()->messageUtil->add('Successfully moved', 'success');
				} catch (Exception $e) {
					Yii::app()->messageUtil->add($e->getMessage(), 'error');
					$transaction->rollback();
				}
			}
		}
	}

	public function init() {
		if ($this->criteria == null) {
			$this->criteria = new CDbCriteria();
		}

		$this->filter();
		$this->filter = $this->model;

		// Если передана переменная delete то попытаемся удалить строку
		if (isset($_GET['delete'])) {
			if ( $this->model->deleteRow() ) {
				Yii::app()->messageUtil->add('Successfully deleted', 'success', 'model');
			} else {
				Yii::app()->messageUtil->add('Error in the removal process', 'error', 'model');
			}
			//TODO: Всетаки нужно редиректить
			//Yii::app()->request->redirect(Yii::app()->controller->createUrl('list'));
		}

		$this->registerAssets();

		$this->updateParentCriteria();

		$this->addIdColl();

		//Есть ли в таблице колонка 'position'
		if(array_search('position', $this->model->tableSchema->columnNames) !== false){
			$this->upDownRow();
			//Добавляем кнопочки редактирования
			$this->columns = array_merge($this->columns,
				array($this->getButtonsConfig('{up} {down} ' . $this->buttonConfig)));
			//Отключаем сортировку
			$this->enableSorting = false;
			$this->criteria->order = 'position ASC';
		}else{
			//Включаем сортировку
			$this->enableSorting = true;
			//Добавляем кнопочки редактирования
			$this->columns = array_merge($this->columns,array($this->getButtonsConfig($this->buttonConfig)));
		}

		$this->dataProvider = new CActiveDataProvider($this->model, array(
			'criteria' => $this->criteria,
			'pagination' => array(
				'class' => 'GridWidgetPagination',
			)
		));

		parent::init();
	}

	/**
	 * Применяем переданный пользователем фильтр
	 */
	public function filter() {
//		var_dump($_GET);
		$modelClassName = get_class($this->model);
		if (isset($_GET[$modelClassName])) {
			foreach ($_GET[$modelClassName] AS $k => $v) {
				$this->model->$k = $v;
				$this->criteria->compare('t.' . $k, trim($v), true);
			}
		}
	}

	/**
	 * Переопределяем метод вывода информации о показуемых элемнтах
	 * В прошлом варианте информация выводилась лишь в том случае когда выведен хоятбы один элемент,
	 * делаем так что бы иформация выводилась всегда
	 * Добавли ссылку на добавление элемента в таблицу
	 * Добавли селектор для выбора количества элементов выводимых на страницу
	 */
	public function renderSummary() {
		// В прошлом варианте summary не выводилось если количество записей == 0, сейчас выводится всегда
		$count = $this->dataProvider->getItemCount();

		echo '<div class="'.$this->summaryCssClass.'">';
		if($this->enablePagination) {
			$pagination=$this->dataProvider->getPagination();
			$total=$this->dataProvider->getTotalItemCount();
			$start=$pagination->currentPage*$pagination->pageSize+1;
			$end=$start+$count-1;
			if($end>$total) {
				$end=$total;
				$start=$end-$count+1;
			}
			if(($summaryText=$this->summaryText)===null)
				$summaryText=Yii::t('zii','Displaying {start}-{end} of 1 result.|Displaying {start}-{end} of {count} results.',$total);
			echo strtr($summaryText,array(
				'{start}'=>$start,
				'{end}'=>$end,
				'{count}'=>$total,
				'{page}'=>$pagination->currentPage+1,
				'{pages}'=>$pagination->pageCount,
			));
		} else {
			if(($summaryText=$this->summaryText)===null)
				$summaryText=Yii::t('zii','Total 1 result.|Total {count} results.',$count);
			echo strtr($summaryText,array(
				'{count}'=>$count,
				'{start}'=>1,
				'{end}'=>$count,
				'{page}'=>1,
				'{pages}'=>1,
			));
		}

		//Добавляем кнопочку Добавления в таблицу
		echo ' <a href="'.Yii::app()->controller->createUrl('update',array('id'=>0)).'">' . Yii::t('editor', 'Add entry'). '</a>';

		// Добавляем селектор для выбора количества строк на страницу
		echo CHtml::tag('div', array('style' => 'float:right;'), $this->dataProvider->getPagination()->renderPageSizeSelect());
		echo '</div>';
	}

	/**
	 * Генерирует содержимое таблици
	 * Если модель иерархическая, то добаляет ссылку перехода на уровень выше
	 */
	public function renderTableBody() {
		$data = $this->dataProvider->getData();
		$n = count($data);
		echo "<tbody>\n";

		if ($this->parentRelationName !== false && isset($_GET['parentId'])) {
			$model = $this->model;
			$element = $model::model()->find(array(
				'condition' => 't.id = :parentId',
				'params' => array(':parentId' => $_GET['parentId']),
				'with' => $this->parentRelationName,
			));
			// Если элемент не найден сгенерируем ссылку на первый уровень
			if ($element !== null) {
				$url = self::createParentUrl($element->{$this->parentRelationName});
			} else {
				$url = self::createParentUrl(null);
			}
			echo '<tr><td colspan="'.count($this->columns).'" class="topLevel">';
			echo CHtml::link('&uarr; на уровень выше &uarr;', $url);
			echo "</td></tr>\n";
		}

		if($n>0) {
			for($row = 0;$row < $n;++$row)
				$this->renderTableRow($row);
		} else {
			echo '<tr><td colspan="'.count($this->columns).'" class="empty">';
			$this->renderEmptyText();
			echo "</td></tr>\n";
		}
		echo "</tbody>\n";
	}

	/**
	 * Метод создание ссылки для перехода на уровень иерархии
	 * @param $element
	 * @return string
	 */
	public static function createParentUrl($element) {
		$params = $_GET;
		unset($params['page']);
		if ($element !== null) {
			return Yii::app()->createUrl(Yii::app()->controller->route, array_merge(
				$params, array('parentId' => $element->id)
			));
		} else {
			unset($params['parentId']);
			return Yii::app()->createUrl(Yii::app()->controller->route, $params);
		}
	}

	/**
	 * Если в модели иерархическая то отображаем записи только одного уровня,
	 * того на котором находимся в данный момент
	 * Также добавлем столбец перехода следующий уровень
	 */
	protected function updateParentCriteria() {
		if ($this->parentRelationName === false) {
			return;
		}

		$colConfig = array(
			'class' => 'CLinkColumn',
			'imageUrl' => $this->baseAssetsUrl . '/img/folder.png',
			'urlExpression' => 'GridWidget::createParentUrl($data)',
			'htmlOptions' => array(
				'style' => 'width:1%;'
			),
		);

		$this->columns = array_merge(array($colConfig), $this->columns);

		$relationField = $this->model->relations()[$this->parentRelationName][2];
		if ( ! isset($_GET['parentId'])) {
			$this->criteria->addCondition($relationField . ' IS NULL');
		} else {
			$this->criteria->addCondition($relationField . ' = :parentId');
			$this->criteria->params += array(':parentId' => $_GET['parentId']);
		}
	}

	/**
	 * Добавление столбца ID, в начало вывода
	 */
	protected function addIdColl() {
		$config = array(
			'name'=>'id',
			'htmlOptions' => array(
				'style'=>'width:1%;',
			),
		);
		$this->columns = array_merge(array($config), $this->columns);
	}

	/**
	 * Подключаем необходимые файлы javascript и css
	 */
	private function registerAssets() {
		Yii::app()->clientScript->registerCoreScript('jquery');
		Yii::app()->clientScript->registerCoreScript('jquery.ui');

		Yii::app()->clientScript->registerCssFile(
			Yii::app()->clientScript->getCoreScriptUrl() . '/jui/css/base/jquery-ui.css');

		$this->baseAssetsUrl = Yii::app()->assetManager->publish(
			Yii::getPathOfAlias('yii-crud.assets.*'), true, -1, YII_DEBUG
		);
		$this->cssFile = $this->baseAssetsUrl . '/css/grid.css';
		Yii::app()->clientScript->registerScriptFile($this->baseAssetsUrl . '/js/grid.js');
	}
}
