<?php
/**
 * Created: 01.02.13 15:31
 * 
 * @author tcore
 */

interface IInput {
	/**
	 * Может ли элемент принимать булевское значение
	 * @return bool
	 */
	public function getAllowNull();

	/**
	 * Указывавет может ли элемент принимать булевское значение
	 * @param bool $allowNull
	 */
	public function setAllowNull($allowNull);

	/**
	 * Проверяет записано ли значение null в элемент или нечто другое
	 * @return bool
	 */
	public function isNull();

	/**
	 * Устанавливает модель для элемента
	 * @param CActiveRecord $model
	 */
	public function setModel($model);

	/**
	 * Возвращает модель элемента
	 * @return CActiveRecord
	 */
	public function getModel();

	/**
	 * Указывает многоязычное это поле или нет
	 * @param bool $multilingual
	 */
	public function setMultilingual($multilingual);

	/**
	 * Возвращает многоязычное это поле или нет
	 * @return bool
	 */
	public function getMultilingual();

	/**
	 * Создает єлемент
	 * @param strung|null $language
	 * @param array $params
	 * @return string
	 */
	public function createInput($language=null, $params=array());

	/**
	 * Создает полностью готовый многоязычный элемент, если настройка установлена многоязычность
	 * если многоязычность отключена то просто создает элемент
	 * @return string
	 */
	public function input();

	/**
	 * Сохраняет элемент в модель
	 * @return CActiveRecord
	 */
	public function save();

	/**
	 * Имя поля по которому в моделе можно обратиться к текущему элементу
	 * @return string
	 */
	public function getModelFieldName();

	public function setConfig($config);
}
