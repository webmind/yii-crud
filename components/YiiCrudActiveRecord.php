<?php

abstract class YiiCrudActiveRecord extends CActiveRecord{

	public abstract function getDeleteConfig();

	/**
	 * Удаляет запись с таблици и все с ней связанные поля
	 * @return bool
	 */
	public function deleteRow() {
		if(isset($_GET['delete'])){
			$transaction = Yii::app()->db->beginTransaction();
			try{
				$this->doDelete();
				$transaction->commit();
				return true;
			}catch(Exception $e){
				$transaction->rollback();
				if ($e->getMessage() !== '') {
					Yii::app()->messageUtil->add($e->getMessage(), 'error');
				}
				return false;
			}
		}else{
			return false;
		}
	}

	protected function doDelete($item=null) {
		if($item===null){
			$item = $this->findByPk((int)$_GET['delete']);
			if ($item === null) {
				Yii::app()->messageUtil->add('Record with this id does not exist', 'error', 'model');
				throw new CException();
			}
		}
		$deleteConfig = $this->getDeleteConfig();
		foreach($deleteConfig AS $v){
			if(is_array($item->$v)){
				foreach($item->$v AS $vEl){
					$vEl->doDelete($vEl);
				}
			}else{
				if($item->$v!==null){
					$item->$v->doDelete($item->$v);
				}
			}
		}
		if ( ! $item->delete()) {
			Yii::app()->messageUtil->add($item->getErrors(), 'error');
			throw new CException();
		}
	}

	public function beforeDelete() {
		if (isset($this->getTableSchema()->columns['system_flag'])) {
			if ($this->system_flag == 1) {
				$this->addError('system_flag', Yii::t('model', 'This is protected string, it can not be removed'));
				return false;
			}
		}
		return true;
	}

	public function behaviors(){
		return array(
			'CAdvancedArBehavior' => array(
				'class' => 'yii-crud.components.CAdvancedArBehavior'
			)
		);
	}
}
