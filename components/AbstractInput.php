<?php
/**
 * Поддерживает свойства: allowNull, multilingual
 */
abstract class AbstractInput extends CWidget implements IInput{

	/**
	 * @var CActiveRecord модель редактируемой таблици
	 */
	private $model;

	/**
	 * @var bool разрешить записывать в БД значение Null
	 */
	protected $allowNull = false;

	/**
	 * @var bool является ли поле многоязычным
	 */
	private $isMultilingual = false;

	/**
	 * @return bool
	 */
	public function isNull() {
		if ($this->getAllowNull() === true) {
			if ($this->model->{$this->getModelFieldName()} === null
			|| $this->model->{$this->getModelFieldName()} === array()
			|| $this->model->{$this->getModelFieldName()} === '') {
				return true;
			} else {
				return false;
			}
		} else {
			return false;
		}
	}

	/**
	 * Возвращает сегенерированный HTML элемента, если элемент многоязычный, то делает его таким
	 * @return string
	 */
	public function input() {
		// Если элемент многоязычный
		if ($this->getMultilingual()) {
			return $this->renderFile(
				Yii::getPathOfAlias('yii-crud.views') . '/multilingual.php', null, true);
		}
		return $this->createInput();
	}

	/**
	 * @return bool
	 */
	public function getAllowNull() {
		if ($this->allowNull === true) {
			if (isset($this->model->tableSchema->columns[$this->getModelFieldName()])) {
				return $this->model->tableSchema->columns[$this->getModelFieldName()]->allowNull;
			} else {
				return true;
			}
		} else {
			return false;
		}
	}

	/**
	 * @param boolean $allowNull
	 */
	public function setAllowNull($allowNull)
	{
		$this->allowNull = $allowNull;
	}

	/**
	 * @return CActiveRecord
	 */
	public function save() {
		$columnName = $this->getModelFieldName();
		// Если пользователь хочет записать в поле значени null
		$setNull = false;
		if (isset($_POST['editor']['is_null'][$this->getModelFieldName()])
			&& $_POST['editor']['is_null'][$this->getModelFieldName()] == 1
			&& $this->getAllowNull()) {
			$setNull = true;
		}
		if ($this->getMultilingual()) {
			$languages = explode(',', Yii::app()->params['languages']);
			foreach($languages as $languageName) {
				$langColumnName = $columnName . '_' . $languageName;
				if ($setNull) {
					$this->getModel()->$langColumnName = null;
				} else {
					// Если передано пустое значение и разрешена запись null - то запишем null
					if ($this->getAllowNull() && $_POST['editor'][$langColumnName] == '') {
						$this->getModel()->$langColumnName = null;
					} else {
						$this->getModel()->$langColumnName = $_POST['editor'][$langColumnName];
					}
				}
			}
		} else {
			if ($setNull) {
				$this->getModel()->$columnName = null;
			} else {
				// Если передано пустое значение и разрешена запись null - то запишем null
				if ($this->getAllowNull() && $_POST['editor'][$columnName] == '') {
					$this->getModel()->$columnName = null   ;
				} else {
					$this->getModel()->$columnName = $_POST['editor'][$columnName];
				}
			}
		}
		return $this->getModel();
	}

	/**
	 * @return CActiveRecord
	 */
	public function afterSave() { }

	/**
	 * Метод который выполняеться после сохранения основной модели
	 * @internal param \CActiveRecord $model
	 * @return CActiveRecord
	 */
	public function afterMainModelSave() {
		return $this->getModel();
	}

	public function getModelFieldName() {
		if (method_exists($this, 'getColumnName')) {
			return $this->getColumnName();
		}
		if (method_exists($this, 'getRelationName')) {
			return $this->getRelationName();
		}
		throw new Exception("Cant get model field name");
	}

	/**
	 * @return bool
	 */
	public function getMultilingual() {
		return $this->isMultilingual;
	}

	/**
	 * @param boolean $isMultilingual
	 */
	public function setMultilingual($isMultilingual)
	{
		$this->isMultilingual = $isMultilingual;
	}

	/**
	 * @return CActiveRecord
	 */
	public function getModel() {
		return $this->model;
	}

	/**
	 * @param CActiveRecord $model
	 */
	public function setModel($model) {
		$this->model = $model;
	}

	public function setConfig($config) {
		foreach ($config as $k => $v) {
			$methodName = 'set' . ucfirst($k);
			$this->$methodName($v);
		}
	}
}
