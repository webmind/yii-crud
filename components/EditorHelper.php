<?php
/**
 * Created: 08.02.13 23:05
 * 
 * @author tcore
 */
 
class EditorHelper extends CHtml{

	/**
	 * Переопределенный метод создания активного лейбла в котором учитана многоязычность и родительские лейблы
	 * @param CModel $model
	 * @param string $attribute
	 * @param array $htmlOptions
	 * @param bool $multilingual
	 * @return string
	 */
	public static function activeLabel($model,$attribute,$htmlOptions=array(), $multilingual=false) {
		// Если поле указано как не многоязычное возвращаем обычный activeLabel
		if ( ! $multilingual) {
			return parent::activeLabel($model, $attribute, $htmlOptions);
		}
		// Если опция label = false то вернем пустую строку
		if(isset($htmlOptions['label'])) {
			if(($label=$htmlOptions['label'])===false)
				return '';
			unset($htmlOptions['label']);
		} else {
			// Берем текст названия с описания модели
			$label=$model->getAttributeLabel($attribute);
		}
		// Разбиваем в масив языки системы
		$languages = explode(',', Yii::app()->params['languages']);
		// Проверяем ошибки для полей всех языков
		foreach($languages as $v) {
			// Если находим ошибку выделяем label
			if($model->hasErrors($attribute . '_' . $v)) {
				self::addErrorCss($htmlOptions);
			}
		}
		// Проверям ошибки поля без уточнеиня языка
		if ($model->hasErrors($attribute)) {
			self::addErrorCss($htmlOptions);
		}
		// Возвращаем обычный label
		return self::label($label, '', $htmlOptions);
	}


	/**
	 * @param bool | array $a
	 * @return string
	 */
	public static function jsonEncode($a=false)
	{
		if (is_null($a)) return 'null';
		if ($a === false) return 'false';
		if ($a === true) return 'true';
		if (is_scalar($a))
		{
			if (is_float($a))
			{
				// Always use "." for floats.
				$a = str_replace(",", ".", strval($a));
			}
			// All scalars are converted to strings to avoid indeterminism.
			// PHP's "1" and 1 are equal for all PHP operators, but
			// JS's "1" and 1 are not. So if we pass "1" or 1 from the PHP backend,
			// we should get the same result in the JS frontend (string).
			// Character replacements for JSON.
			static $jsonReplaces = array(array("\\", "/", "\n", "\t", "\r", "\b", "\f", '"'),
				array('\\\\', '\\/', '\\n', '\\t', '\\r', '\\b', '\\f', '\"'));
			return '"' . str_replace($jsonReplaces[0], $jsonReplaces[1], $a) . '"';
		}
		$isList = true;
		for ($i = 0, reset($a); $i < count($a); $i++, next($a))
		{
			if (key($a) !== $i)
			{
				$isList = false;
				break;
			}
		}
		$result = array();
		if ($isList)
		{
			foreach ($a as $v) $result[] = self::jsonEncode($v);
			return '[ ' . join(', ', $result) . ' ]';
		}
		else
		{
			foreach ($a as $k => $v) $result[] = self::jsonEncode($k).': '.self::jsonEncode($v);
			return '{ ' . join(', ', $result) . ' }';
		}
	}

	/**
	 * @param $array
	 * @param $varName
	 * @param bool $sub
	 * @return string
	 */
	public static function arrayToJSObject($array, $varName, $sub = false )
	{
		$jsArray = $sub ? $varName . "{" : $varName . " = {";
		$varName = "$varName";
		reset ($array);

		// Loop through each element of the array
		while (list($key, $value) = each($array)) {
			$jsKey = "'" . $key . "':";

			if (is_array($value)) {
				// Multi Dimensional Array
				$temp[] = arrayToJSObject($value, $jsKey, true);
			} else {
				if (is_numeric($value)) {
					$jsKey .= "$value";
				} elseif (is_bool($value)) {
					$jsKey .= ($value ? 'true' : 'false') . "";
				} elseif ($value === NULL) {
					$jsKey .= "null";
				} else {
					static $pattern = array("\\", "'", "\r", "\n");
					static $replace = array('\\', '\\\'', '\r', '\n');
					$jsKey .= "'" . str_replace($pattern, $replace, $value) . "'";
				}
				$temp[] = $jsKey;
			}
		}
		$jsArray .= implode(',', $temp);

		$jsArray .= "}\n";
		return $jsArray;
	}

	public static function delTree($dir) {
		$files = array_diff(scandir($dir), array('.','..'));
		foreach ($files as $file) {
			(is_dir("$dir/$file")) ? delTree("$dir/$file") : unlink("$dir/$file");
		}
		return rmdir($dir);
	}
}
